package uk.ac.reading.student.nv010600.androidgame.game;
import android.content.Context;
import android.util.TypedValue;

import uk.ac.reading.student.nv010600.androidgame.base.Vector2;


public class InitRes {

    // ----- Public ----------------------------------------------------------------------------- //
    public InitRes( Context context ) {
        mContext = context;
    }


    public Vector2 getVector2( int resourceID ) {
        return new Vector2( mContext.getResources().getString( resourceID ) );
    }


    public float getFloat( int resourceID ) {
        TypedValue temp = new TypedValue();
        mContext.getResources().getValue( resourceID, temp, true);
        return temp.getFloat();
    }


    public int getInt( int resourceID ) {
        return mContext.getResources().getInteger( resourceID );
    }


    // ----- Private ---------------------------------------------------------------------------- //
    private Context mContext;
}
