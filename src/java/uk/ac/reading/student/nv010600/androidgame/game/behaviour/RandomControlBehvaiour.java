package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.PauseMovementAttributes;


public class RandomControlBehvaiour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public RandomControlBehvaiour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mMovement = parent.getAttribute( MovementAttributes.class );
        mPause = parent.getAttribute( PauseMovementAttributes.class );
        mLatch = false;
    }


    @Override
    public void update( float elapsed ) {
        if( mLatch ) return;    // once set, do not repeat

        // if parent has pause behaviour, then only activate after pause
        if( mPause != null && mPause.timer() < mPause.delay() + mPause.pauseLength() ) return;

        // set direction randomly (downwards)
        mLatch = true;
        float x = -1 + (float)(Math.random() * 2);                                  // range (-1 -> 1)
        float y = (float)Math.random() * (mMovement.direction().y < 0 ? -1 : 1);    // range ( 0 -> 1), or (-1 -> 0)
        mMovement.setDirection( x, y );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient MovementAttributes mMovement;
    private transient PauseMovementAttributes mPause;
    private transient boolean mLatch;
}
