package uk.ac.reading.student.nv010600.androidgame.base;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;


/**
 *
 * NOTES:
 *  May be able to remove/private the behaviour has/get as behaviours should be independent and
 *  act unaware of all other behaviour modules, only accessing common attributes
 *
 */

/**
 * Core of the ECS pattern. Generic entity where behaviours and properties are populated through
 * Behaviour and Attribute sub-classes.
 * Tracks all existing valid Entity classes.
 *
 * @author Max Denning
 */
public class Entity implements Serializable {

    // ----- Public ----------------------------------------------------------------------------- //

    /**
     * Constructor, initialises member variables
     * @param context context
     */
    public Entity( Context context ) {
        mBehaviours = new HashMap<>();
        mAttributes = new HashMap<>();
        mValid = false;
        mContext = context;
        if( ENTITIES_ALL_LAUNCHED == null ) ENTITIES_ALL_LAUNCHED = new HashSet<>();
        if( ENTITIES_TO_LAUNCH == null ) ENTITIES_TO_LAUNCH = new HashSet<>();
        if( ENTITIES_TO_DESTROY == null ) ENTITIES_TO_DESTROY = new HashSet<>();
    }


    /**
     * Initialises all behaviours
     */
    public final void initialise() {
        // stops errors in behaviour constructors where getAttribute returns null, due to add order
        // ... in entity constructor
        // made final due as override would break everything
        for( Behaviour behaviour : mBehaviours.values() ){
            behaviour.initialise();
        }
    }


    /**
     * Updates all behaviours
     * @param elapsed time since last update call
     */
    public final void update( float elapsed ) {
        // made final as override is unnecessary, all behaviour should be in behaviours & attributes
        for( Behaviour behaviour : mBehaviours.values() ){
            behaviour.update( elapsed );
        }
    }


    /**
     * Updates the lists containing all valid/launched entities. Manages destruction and launching.
     * @param elapsed time since last update call
     */
    public static void updateAll( float elapsed ) {
        //Log.d( "Entity", String.format( "updateAll %d %d %d", ENTITIES_ALL_LAUNCHED.size(), ENTITIES_TO_LAUNCH.size(), ENTITIES_TO_DESTROY.size() ) );

        for( Entity entity : ENTITIES_TO_DESTROY ){
            ENTITIES_ALL_LAUNCHED.remove( entity );
            entity.clear();
        }
        ENTITIES_TO_DESTROY.clear();

        for( Entity entity : ENTITIES_TO_LAUNCH ){
            ENTITIES_ALL_LAUNCHED.add( entity );
        }
        ENTITIES_TO_LAUNCH.clear();


        for( Entity entity : ENTITIES_ALL_LAUNCHED ){
            if( !entity.isValid() ) ENTITIES_TO_DESTROY.add( entity );
            entity.update( elapsed );
        }
    }


    /**
     * Resets list of all valid/launched entities
     */
    public static void clearAll() {
        ENTITIES_TO_DESTROY.clear();

        for( Entity entity : ENTITIES_TO_LAUNCH ){
            entity.clear();
        }
        ENTITIES_TO_LAUNCH.clear();

        for( Entity entity : ENTITIES_ALL_LAUNCHED ){
            entity.clear();
        }
        ENTITIES_ALL_LAUNCHED.clear();
    }


    /**
     * Launches an entity, initialising its behaviours and adding it to tracked list
     * @param entity the entity to launch
     * @param <T> the type of entity, if subclassed
     * @return the entity launched
     */
    public static <T extends Entity> T launch( T entity ) {
        return launch( entity, entity.context() );
    }


    /**
     * Launches the entity using the given context
     * @param entity the entity to launch
     * @param context the context to be applied to the entity
     * @param <T> the type of entity, if subclassed
     * @return
     */
    public static <T extends Entity> T launch( T entity, Context context ) {
        entity.setContext( context );
        entity.initialise();
        ENTITIES_TO_LAUNCH.add( entity );
        entity.setValid( true );
        return entity;
    }


    /**
     * Marks the entity for removal from the tracked list. Invalidates.
     */
    public void destroy() {
        ENTITIES_TO_DESTROY.add( this );
        setValid( false );
        //Log.d( "Entity", String.format( "Destroyed, remaining: %d", entityCount() ) );
    }


    /**
     * Adds the given attributes to the entity object's attribute set. Only one of each type is
     * permitted.
     * @param attribute the attribute to add
     * @param <T> the type of attribute, when subclassed
     * @return the attribute added, or null if unsuccessful
     */
    public <T extends Attribute> T addAttribute( T attribute ) {
        if( mAttributes.containsKey( attribute.getClass() ) ){
            return null;
        }
        mAttributes.put( attribute.getClass(), attribute );
        return (T)getAttribute( attribute.getClass() );
    }


    /**
     * Removes the given attribute
     * @param key class type of the attribute to remove
     * @param <T> the type of attribute, when subclassed
     * @return true, if successful
     */
    public <T extends Attribute> boolean removeAttribute( Class<T> key ) {
        if( !hasAttribute( key ) ) return false;
        mAttributes.remove( key );
        return true;
    }


    /**
     * Determines if the entity instance contains the attribute in question
     * @param key class type of the attribute to remove
     * @param <T> the type of attribute, when subclassed
     * @return true, if successful
     */
    public <T extends Attribute> boolean hasAttribute( Class<T> key ) {
        return mAttributes.containsKey( key );
    }


    /**
     * Gets the attribute in question
     * @param key class type of the attribute to remove
     * @param <T> the type of attribute, when subclassed
     * @return the attribute instance held by the entity, or null if unsuccessful
     */
    public <T extends Attribute> T getAttribute( Class<T> key ) {
        if( !hasAttribute( key ) ) return null;
        return (T)mAttributes.get( key );
    }


    /**
     * Adds the given behaviour to the entity object's behaviour set. Only one of each type is
     * permitted.
     * @param behaviour the behaviour to add
     * @param <T> the type of behaviour, when subclassed
     * @return the behaviour added, or null if unsuccessful
     */
    public <T extends Behaviour> T addBehaviour( T behaviour ) {//throws InvalidKeyException {
        if( mBehaviours.containsKey( behaviour.getClass() ) ){
            //throw new InvalidKeyException( "Given behaviour is already present" );
            return null;
        }
        mBehaviours.put( behaviour.getClass(), behaviour );
        return (T)mBehaviours.get( behaviour.getClass() );
    }


    /**
     * Removes the behaviour in question
     * @param key class type of the behaviour to remove
     * @param <T> the type of behaviour, when subclassed
     * @return true, if successful
     */
    public <T extends Behaviour> boolean removeBehaviour( Class<T> key ) {
        if( !mBehaviours.containsKey( key ) ) return false;
        mBehaviours.remove( key );
        return true;
    }



    // ----- Protected -------------------------------------------------------------------------- //
    protected void setValid( boolean v ) {
        mValid = v;
    }



    // ----- Private ---------------------------------------------------------------------------- //
    /** Attribute instances held by this entity. Properties to be manipulated by behaviours. */
    private HashMap<Class<? extends Attribute>, Attribute> mAttributes;

    /** Behaviour instances held by this entity. Manipulate attributes. */
    private HashMap<Class<? extends Behaviour>, Behaviour> mBehaviours;

    /** The validity of the entity, false destroyed or not launched. */
    private boolean mValid;

    /** @see Context */
    private transient Context mContext;

    /** All launched/valid entities */
    private transient static HashSet<Entity> ENTITIES_ALL_LAUNCHED = null;         // static as only one instance of game running at a time

    /** All entities waiting to be launched */
    private transient static HashSet<Entity> ENTITIES_TO_LAUNCH = null;

    /** All invalid entities waiting to be destroyed */
    private transient static HashSet<Entity> ENTITIES_TO_DESTROY = null;


    /**
     * Clears all containers and destroys behaviours
     */
    private void clear() {
        for( Behaviour behaviour : mBehaviours.values() ) behaviour.destroy();
        mAttributes.clear();
        mBehaviours.clear();
    }


    // ----- Public Accessors ------------------------------------------------------------------- //
    public final boolean isValid() { return mValid; }
    public final Context context() { return mContext; }
    public static int entityCount() { return ENTITIES_ALL_LAUNCHED.size() + ENTITIES_TO_LAUNCH.size() - ENTITIES_TO_DESTROY.size(); }
    public static final HashSet<Entity> allEntities() { return ENTITIES_ALL_LAUNCHED; }

    public final void setContext( Context context ) { mContext = context; }
}
