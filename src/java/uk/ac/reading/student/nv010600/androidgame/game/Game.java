package uk.ac.reading.student.nv010600.androidgame.game;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ScoreAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.TouchControlAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.GraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.entities.PlayerEntity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class Game extends GameThread {

    // ----- Public ----------------------------------------------------------------------------- //
    public Game( Context context, SurfaceHolder holder, Handler handler, LevelManager levelManager ) {
        super( context, holder, handler );

        mLevelManager = levelManager;
        mLevelManager.setContext( mContext );
        mBackground = new ScrollingBackground( mContext );
        mPlayer = null;

        ftimer = 0;
        frames = 0;

        mUITextMedium = new Paint( Paint.FAKE_BOLD_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG );
        mUITextMedium.setTextSize( Display.dp2px( 24 ) );
        mUITextMedium.setTypeface( Typeface.MONOSPACE );
        mUITextMedium.setColor( Color.BLACK );
        mUITextMedium.setStyle( Paint.Style.FILL_AND_STROKE );

        mUITextSmall = new Paint( mUITextMedium );
        mUITextSmall.setTextSize( Display.dp2px( 16 ) );

        mUITextLarge = new Paint( mUITextMedium );
        mUITextLarge.setTextSize( Display.dp2px( 32 ) );
        mUITextLarge.setTextAlign( Paint.Align.CENTER );

        mUIHitpointsBounds = new Rect();
        mUITextMedium.getTextBounds( "0000", 0, 4, mUIHitpointsBounds );

        mUIWinBounds = new Rect();
        mUITextLarge.getTextBounds( mContext.getResources().getString( R.string.game_win_message ), 0, mContext.getResources().getString( R.string.game_win_message ).length(), mUIWinBounds );

        mEndTimer = -1;
        mEndTimerMax = 2;
    }


    /**
     * Set up player character, and initialise level manager
     */
    @Override
    public void initialise() {
        mTouchInput = new TouchControlAttributes();
        mPlayer = Entity.launch( new PlayerEntity( mContext, new Vector2( Display.widthPx()*0.5f, Display.heightPx()*0.75f ), mTouchInput ) );
        mLevelManager.resetTimer();
    }



    @Override
    public void cleanup() {
        super.cleanup();
        ScoreAttributes.setTotalScore( 0 );
        Entity.clearAll();
    }


    /**
     * Touch listener. Record touch events.
     * @param v view on which touch was registered
     * @param e motion event generated by touch
     * @return true if handled
     */
    @Override
    public boolean onTouch( View v, MotionEvent e ){
        if( super.onTouch( v, e ) || getMode() != STATE_RUNNING ) return true;

        mTouchInput.setLastEvent( e );  // allow player to move
        return true;
    }



    // ----- Protected -------------------------------------------------------------------------- //

    /**
     * Updates all aspects of the game: level manager, entities, pause/win/lose state
     * @param elapsed time since last update call
     */
    @Override
    protected void update( float elapsed ) {
        mLevelManager.update( elapsed );
        Entity.updateAll( elapsed );


        if( mLevelManager.isComplete() && getMode() != STATE_WIN ){
            if( mEndTimer < 0 )                  mEndTimer = 0;
            else if( mEndTimer >= mEndTimerMax ) { setState( STATE_WIN ); Log.d( "Game", "STATE_WIN" ); }
            else                                 mEndTimer += elapsed;
        }

        synchronized( mLock ){
            if( mPlayer.getAttribute( HealthAttributes.class ).hitPoints() <= 0 ) setState( STATE_LOSE );
        }

        // FPS Counter
        ftimer += elapsed;
        frames++;
        if( ftimer > 3.0f ){
            Log.d( "Game", String.format( " ----- FPS: %f ----- ", frames/ftimer ) );
            ftimer = ftimer % 3.0f;
            frames = 0;
        }
    }


    /**
     * Draws all aspects of the game to the canvas: background, entities, user interface
     * @param canvas canvas upon which to draw
     */
    @Override
    protected void draw( Canvas canvas ) {
        if( canvas == null ) return;

        // Clear & draw background
        canvas.drawColor( 0, PorterDuff.Mode.CLEAR );
        canvas.drawColor( Color.WHITE );
        mBackground.draw( canvas );


        // Draw all entities
        GraphicsBehaviour.drawAll( canvas );


        // Draw user interface on top
        int hitpoints = 0, maxHitpoints = 0;
        synchronized( mLock ){
            if( mPlayer != null ){
                hitpoints = mPlayer.getAttribute( HealthAttributes.class ).hitPoints();
                maxHitpoints = mPlayer.getAttribute( HealthAttributes.class ).maxHitPoints();
            }
        }
        canvas.drawText( String.format( "%06d", ScoreAttributes.totalScore() ), Display.widthPx() - ((float)(mUIHitpointsBounds.width()*1.5) + Display.dp2px(10)), Display.dp2px(10) - mUITextMedium.getFontMetrics().top, mUITextMedium );
        canvas.drawText( String.format( "%04d", hitpoints ), Display.dp2px(10), Display.dp2px(10) - mUITextMedium.getFontMetrics().top, mUITextMedium );
        canvas.drawText( String.format( "/%04d", maxHitpoints ), mUIHitpointsBounds.right + Display.dp2px(12), Display.dp2px(10) - mUITextMedium.getFontMetrics().top, mUITextSmall );


        // Draw lose/win/pause state, if applicable
        if( getMode() != STATE_RUNNING ){
            canvas.drawColor( Color.LTGRAY, PorterDuff.Mode.MULTIPLY );
            String display_str = "";

            switch( getMode() ){
                case STATE_LOSE: display_str = mContext.getResources().getString( R.string.game_lose_message );
                    break;

                case STATE_WIN:
                    display_str = mContext.getResources().getString( R.string.game_win_message );
                    canvas.drawText( String.format( "%06d", ScoreAttributes.totalScore() ), (canvas.getWidth()/2f) - (mUIWinBounds.width()/3f), (canvas.getHeight()/2f) + (mUIWinBounds.height()/2f) - mUITextLarge.getFontMetrics().bottom - mUITextMedium.getFontMetrics().top, mUITextMedium );
                    break;

                case STATE_PAUSE:
                    display_str = mContext.getResources().getString( R.string.game_pause_message );
                    break;

                default: break;
            }

            canvas.drawText( display_str, canvas.getWidth()/2f, canvas.getHeight()/2f, mUITextLarge );
        }
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private TouchControlAttributes mTouchInput;
    private Entity mPlayer;

    private LevelManager mLevelManager;
    private float mEndTimer;
    private float mEndTimerMax;

    private float ftimer;
    private float frames;

    private ScrollingBackground mBackground;
    private Paint mUITextMedium;
    private Paint mUITextSmall;
    private Paint mUITextLarge;
    private Rect mUIHitpointsBounds;
    private Rect mUIWinBounds;
    private final Integer mLock = 0;
}
