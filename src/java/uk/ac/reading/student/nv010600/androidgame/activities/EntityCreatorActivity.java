package uk.ac.reading.student.nv010600.androidgame.activities;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.game.InitRes;
import uk.ac.reading.student.nv010600.androidgame.base.LevelBuilder;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.PauseMovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SeekerAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SinusoidControlAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.PauseMovementBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.ProjectileGeneratorBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.RandomControlBehvaiour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.SeekerControlBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.SinusoidControlBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.entities.OpponentEntity;


public class EntityCreatorActivity extends AppCompatActivity implements Button.OnClickListener, Spinner.OnItemSelectedListener {


    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_entity_creator );

        findViewById( R.id.btn_add_weapon ).setOnClickListener( this );
        findViewById( R.id.btn_remove_weapon ).setOnClickListener( this );
        findViewById( R.id.fab_save_entity ).setOnClickListener( this );
        ((Spinner)findViewById( R.id.spn_sprite )).setOnItemSelectedListener( this );

        mWeaponList = (LinearLayout)findViewById( R.id.lst_weapons );
        mSpritePreviewView = (ImageView)findViewById( R.id.imv_sprite_preview );
        mProjectileGenerators = new ArrayList<>();

        mLevelBuilder = (LevelBuilder)getIntent().getExtras().get( "builder" );
        setResult( RESULT_CANCELED );
    }


    @Override
    public void onClick( View source ) {
        switch( source.getId() ){
            case R.id.btn_add_weapon: addProjectileGenerator(); break;
            case R.id.btn_remove_weapon: removeProjectileGenerator(); break;
            case R.id.fab_save_entity: saveAndExit(); break;
            default: break;
        }
    }


    @Override
    public void onItemSelected( AdapterView adapter, View source, int which, long l ) {
        if( mLastSpriteSelected == which ) return;
        mLastSpriteSelected = which;

        if( mSpritePreview != null ) mSpritePreview.recycle();
        int resourceID = 0;

        switch( which ){
            case 0: resourceID = R.drawable.meteor; break;
            case 1: resourceID = R.drawable.opponent1; break;
            case 2: resourceID = R.drawable.opponent2; break;
            case 3: resourceID = R.drawable.opponent3; break;
            case 4: resourceID = R.drawable.opponent4; break;

            default: resourceID = R.drawable.placeholder; break;
        }

        mSpritePreview = BitmapFactory.decodeResource( getResources(), resourceID );
        mSpritePreviewView.setImageBitmap( mSpritePreview );
    }


    @Override
    public void onNothingSelected( AdapterView adapter ) {
        // do nothing
    }


    public void addProjectileGenerator() {
        LayoutInflater inflater = getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder( EntityCreatorActivity.this );

        builder.setView( inflater.inflate( R.layout.dialog_entity_creator_projectile_generator_attributes, null ) )
                .setTitle( "Weapon Attributes" )
                .setCancelable( true )
                .setPositiveButton( "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        int projectile_type = ((Spinner)((AlertDialog)dialog).findViewById( R.id.spn_projectile_type )).getSelectedItemPosition();
                        String projectile_delay = ((EditText)((AlertDialog)dialog).findViewById( R.id.txt_projectile_delay )).getText().toString().trim();
                        String group_delay = ((EditText)((AlertDialog)dialog).findViewById( R.id.txt_projectile_group_delay )).getText().toString().trim();
                        String group_size = ((EditText)((AlertDialog)dialog).findViewById( R.id.txt_projectile_group_size )).getText().toString().trim();
                        int projectile_pattern = ((Spinner)((AlertDialog)dialog).findViewById( R.id.spn_projectile_pattern )).getSelectedItemPosition();
                        String streams = ((EditText)((AlertDialog)dialog).findViewById( R.id.txt_projectile_streams )).getText().toString().trim();
                        String spread = ((EditText)((AlertDialog)dialog).findViewById( R.id.txt_projectile_spread )).getText().toString().trim();
                        String offset_x = ((EditText)((AlertDialog)dialog).findViewById( R.id.txt_projectile_source_offset_x )).getText().toString().trim();
                        String offset_y = ((EditText)((AlertDialog)dialog).findViewById( R.id.txt_projectile_source_offset_y )).getText().toString().trim();

                        if( projectile_delay.isEmpty() || group_delay.isEmpty() || group_size.isEmpty() || streams.isEmpty() || spread.isEmpty() || offset_x.isEmpty() || offset_y.isEmpty() ){
                            Toast.makeText( getBaseContext(), "Invalid Weapon: Not all attributes set", Toast.LENGTH_LONG ).show();
                            return;
                        }

                        ProjectileAttributes.ProjectileType type;
                        switch( projectile_type ){
                            case 0: type = ProjectileAttributes.ProjectileType.LASER; break;
                            case 1: type = ProjectileAttributes.ProjectileType.MISSILE; break;
                            default: type = ProjectileAttributes.ProjectileType.LASER; break;
                        }

                        ProjectileGeneratorAttributes.Pattern pattern;
                        switch( projectile_pattern ){
                            case 0: pattern = ProjectileGeneratorAttributes.Pattern.PARALLEL; break;
                            case 1: pattern = ProjectileGeneratorAttributes.Pattern.RADIAL; break;
                            default: pattern = ProjectileGeneratorAttributes.Pattern.PARALLEL; break;
                        }

                        try{
                            mProjectileGenerators.add( new ProjectileGeneratorAttributes().new Generator(
                                    type, Float.parseFloat( projectile_delay ), Float.parseFloat( group_delay ), Integer.parseInt( group_size ),
                                    Integer.parseInt( streams ), pattern, Float.parseFloat( spread ), new Vector2( offset_x + " " + offset_y ) ) );

                        }catch( NumberFormatException e ){
                            Toast.makeText( getBaseContext(), "Invalid Weapon: Not all attributes formatted correctly", Toast.LENGTH_LONG ).show();
                            return;
                        }


                        TextView tv = new TextView( getBaseContext() );
                        tv.setText( String.format( "%s %s\nDelay (%s), Group Delay (%s), Group Size (%s), Streams (%s), Spread (%s), OffsetX (%s) OffsetY(%s)",
                                                    pattern.name(), type.name(), projectile_delay, group_delay, group_size, streams, spread, offset_x, offset_y ) );
                        tv.setTextColor( Color.BLACK );
                        tv.setPadding( 0, 0, 32, 32 );
                        mWeaponList.addView( tv );
                    }
                });
        builder.show();
    }


    public void removeProjectileGenerator() {
        if( mWeaponList.getChildCount() <= 0 ) return;
        mWeaponList.removeViewAt( mWeaponList.getChildCount()-1 );
    }


    public void saveAndExit() {
        String max_speed = ((EditText)findViewById( R.id.txt_movement_max_speed )).getText().toString().trim();
        String acceleration = ((EditText)findViewById( R.id.txt_movement_acceleration )).getText().toString().trim();
        String hitpoints = ((EditText)findViewById( R.id.txt_hitpoints )).getText().toString().trim();
        String damage = ((EditText)findViewById( R.id.txt_damage )).getText().toString().trim();
        String spawn_time = ((EditText)findViewById( R.id.txt_spawn_time )).getText().toString().trim();
        String spawn_x = ((EditText)findViewById( R.id.txt_spawn_location_x )).getText().toString().trim();
        String spawn_y = ((EditText)findViewById( R.id.txt_spawn_location_y )).getText().toString().trim();         // not required to be filled
        String score = ((EditText)findViewById( R.id.txt_score )).getText().toString().trim();                      // not required to be filled
        int sprite = ((Spinner)findViewById( R.id.spn_sprite )).getSelectedItemPosition();
        int behaviour = ((Spinner)findViewById( R.id.spn_ai_control )).getSelectedItemPosition();

        if( max_speed.isEmpty() || acceleration.isEmpty() || hitpoints.isEmpty() || damage.isEmpty() || spawn_time.isEmpty() || spawn_x.isEmpty() ){
            Toast.makeText( getBaseContext(), "Invalid Entity: Not all attributes set", Toast.LENGTH_LONG ).show();
            return;
        }

        if( spawn_y.isEmpty() ) spawn_y = "-1";
        if( score.isEmpty() ) score = "0";


        LevelBuilder.EntityBuilder builder = mLevelBuilder.buildEntity( Float.parseFloat( spawn_time ), new Vector2( spawn_x + " " + spawn_y ) );
        builder.base( new OpponentEntity( getBaseContext(), Float.parseFloat( max_speed ), Float.parseFloat( acceleration ), Integer.parseInt( hitpoints ), Integer.parseInt( damage ), Integer.parseInt( score ) ) );


        if( !mProjectileGenerators.isEmpty() ){
            builder.add( new ProjectileGeneratorAttributes( mProjectileGenerators ) )
                   .add( ProjectileGeneratorBehaviour.class );
        }


        switch( sprite ){
            case 0: builder.add( new GraphicsAttributes( R.drawable.meteor, new Vector2( getResources().getString( R.string.init_meteor_graphics_size ) ) ) ); break;
            case 1: builder.add( new GraphicsAttributes( R.drawable.opponent1, new Vector2( getResources().getString( R.string.init_opponent1_graphics_size ) ) ) ); break;
            case 2: builder.add( new GraphicsAttributes( R.drawable.opponent2, new Vector2( getResources().getString( R.string.init_opponent2_graphics_size ) ) ) ); break;
            case 3: builder.add( new GraphicsAttributes( R.drawable.opponent3, new Vector2( getResources().getString( R.string.init_opponent3_graphics_size ) ) ) ); break;
            case 4: builder.add( new GraphicsAttributes( R.drawable.opponent4, new Vector2( getResources().getString( R.string.init_opponent4_graphics_size ) ) ) ); break;

            default: builder.add( new GraphicsAttributes( R.drawable.placeholder, new Vector2( getResources().getString( R.string.init_placeholder_graphics_size ) ) ) ); break;
        }

        boolean addWaitBehaviour = false;
        InitRes i = new InitRes( getBaseContext() );
        switch( behaviour ){
            case 0: // None
            default: break;


            case 1: // Seeker
                builder.add( new SeekerAttributes( i.getFloat( R.raw.default_seek_delay ), i.getFloat( R.raw.default_seek_length ), i.getInt( R.integer.default_seek_radius ), i.getFloat( R.raw.default_seek_turning_speed ) ) )
                       .add( SeekerControlBehaviour.class );

            case 2: // Wait
                addWaitBehaviour = true;
                break;


            case 4: // Wait & Sinusoidal
                addWaitBehaviour = true;

            case 3: // Sinusoidal
                builder.add( new SinusoidControlAttributes( i.getFloat( R.raw.default_sinusoid_amplitude ), i.getFloat( R.raw.default_sinusoid_frequency ) ) )
                       .add( SinusoidControlBehaviour.class );
                break;


            case 6: // Wait & Random
                addWaitBehaviour = true;

            case 5: // Random
                builder.add( RandomControlBehvaiour.class );
                break;
        }


        if( addWaitBehaviour ){
            builder.add( new PauseMovementAttributes( i.getFloat( R.raw.default_movement_pause_delay ), i.getFloat( R.raw.default_movement_pause_length ) ) )
                   .add( PauseMovementBehaviour.class );
        }


        builder.create();

        Intent result = new Intent();
        result.putExtra( "builder", mLevelBuilder );
        result.putExtra( "sprite", builder.entity().getAttribute( GraphicsAttributes.class ).resourceID() );
        result.putExtra( "info", String.format( "Spawn Time: %s, Spawn: (%s, %s), Sprite: %d, Behaviour: %d, Max Speed: %s, Acceleration: %s, Hitpoints: %s, Damage: %s, Score: %s",
                                                spawn_time, spawn_x, spawn_y, sprite, behaviour, max_speed, acceleration, hitpoints, damage, score ) );



        setResult( RESULT_OK, result );
        finish();
    }



    private List<ProjectileGeneratorAttributes.Generator> mProjectileGenerators;
    private LevelBuilder mLevelBuilder;

    private LinearLayout mWeaponList;
    private ImageView mSpritePreviewView;
    private Bitmap mSpritePreview;
    private int mLastSpriteSelected;
}
