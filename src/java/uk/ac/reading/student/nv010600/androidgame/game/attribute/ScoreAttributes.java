package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;


public class ScoreAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public ScoreAttributes() {
        this( 0 );
    }

    public ScoreAttributes( int score ) {
        mScore = score;
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private int mScore;
    private transient static int mTotalScore;


    // ----- Public Accessors ------------------------------------------------------------------- //
    public synchronized int score() { return mScore; }
    public synchronized static int totalScore() { return mTotalScore; }

    public synchronized void setScore( int score ) { mScore = score; }

    public synchronized static void setTotalScore( int total ) {
        if( total >= 999999 ) total = 999999;
        mTotalScore = total;
    }
}
