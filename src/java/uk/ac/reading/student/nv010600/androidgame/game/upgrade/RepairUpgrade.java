package uk.ac.reading.student.nv010600.androidgame.game.upgrade;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.R;
import java.util.Random;


public class RepairUpgrade extends Upgrade {

    // ----- Public ----------------------------------------------------------------------------- //
    public RepairUpgrade( int healAmount ) {
        super( R.drawable.upgrade_repair );
        mHealAmount = healAmount;
    }


    @Override
    public void begin( Entity parent ) {
        super.begin( parent );

        // Heal parent by given amount
        if( parent.hasAttribute( HealthAttributes.class ) ){
            HealthAttributes health = parent.getAttribute( HealthAttributes.class );
            health.setHitPoints( health.hitPoints() + mHealAmount );
        }

        markCompleted();
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private int mHealAmount;
}
