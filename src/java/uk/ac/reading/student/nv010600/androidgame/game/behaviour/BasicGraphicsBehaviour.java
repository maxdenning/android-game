package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

//mBall = BitmapFactory.decodeResource (gameView.getContext().getResources(), R.drawable.small_red_ball);
//canvas.drawBitmap(mBall, mBallX - mBall.getWidth() / 2, mBallY - mBall.getHeight() / 2, null);

public class BasicGraphicsBehaviour extends GraphicsBehaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public BasicGraphicsBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        super.initialise();
        mGraphics = parent.getAttribute( GraphicsAttributes.class );
        mMovement = parent.getAttribute( MovementAttributes.class );

        if( mGraphics.resourceID() == 0 ) return;
        Bitmap src = BitmapFactory.decodeResource( parent.context().getResources(), mGraphics.resourceID() );
        mGraphics.setGraphic( Bitmap.createScaledBitmap( src, (int) Display.dp2px( mGraphics.size().x ), (int) Display.dp2px( mGraphics.size().y ), false ) );
    }


    @Override
    public void destroy() {
        super.destroy();

        if( mGraphics.resourceID() != 0 ){
            mGraphics.graphic().recycle();
            mGraphics.setGraphic( null );
        }
        mGraphics.setVisible( false );
    }


    @Override
    public void draw( Canvas canvas ){
        if( !parent.isValid() || !mGraphics.visible() ) return;
        canvas.drawBitmap( mGraphics.graphic(), mMovement.position().x - mGraphics.graphic().getWidth() / 2, mMovement.position().y - mGraphics.graphic().getHeight() / 2, null );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient GraphicsAttributes mGraphics;
    private transient MovementAttributes mMovement;
    //private transient static final Integer mLock = 0;
}
