package uk.ac.reading.student.nv010600.androidgame.activities;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;

import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.LevelBuilder;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;


public class MainMenuActivity extends AppCompatActivity implements Button.OnClickListener {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState );
        setContentView( R.layout.activity_main_menu );

        findViewById( R.id.btn_level_1 ).setOnClickListener( this );
        findViewById( R.id.btn_level_2 ).setOnClickListener( this );
        findViewById( R.id.btn_level_3 ).setOnClickListener( this );
        findViewById( R.id.btn_level_creator ).setOnClickListener( this );
        findViewById( R.id.btn_level_loader ).setOnClickListener( this );
        findViewById( R.id.btn_credits ).setOnClickListener( this );
    }


    @Override
    public void onClick( View source ) {
        switch( source.getId() ){
            case R.id.btn_level_1:
            case R.id.btn_level_2:
            case R.id.btn_level_3: onLevelClick( source );
                break;
            case R.id.btn_level_creator: onLevelCreatorClick( source );
                break;
            case R.id.btn_level_loader: onLevelLoaderClick( source );
                break;
            case R.id.btn_credits: onCreditsClick( source );
                break;
            default: break;
        }
    }


    public void onLevelClick( View source ) {
        Intent gameIntent = new Intent( getBaseContext(), GameActivity.class );

        switch( source.getId() ){
            case R.id.btn_level_1: gameIntent.putExtra( "level", getResources().getString( R.string.level_filename_1 ) );
                break;

            case R.id.btn_level_2: gameIntent.putExtra( "level", getResources().getString( R.string.level_filename_2 ) );
                break;

            case R.id.btn_level_3: gameIntent.putExtra( "level", getResources().getString( R.string.level_filename_3 ) );
                break;

            default: return;
        }

        startActivity( gameIntent );
    }


    public void onLevelCreatorClick( View source ) {
        startActivity( new Intent( getBaseContext(), LevelCreatorActivity.class ) );
    }


    public void onLevelLoaderClick( View source ) {
        final EditText text_input = new EditText( this );

        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( R.string.main_menu_level_loader_title )
               .setMessage( R.string.main_menu_level_loader_message )
                .setView( text_input )
                .setCancelable( true )
                .setPositiveButton("Load", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        String input = text_input.getText().toString();
                        if( input.isEmpty() ) return;

                        Intent gameIntent = new Intent( getBaseContext(), GameActivity.class );
                        gameIntent.putExtra( "level", input );
                        startActivity( gameIntent );
                    }
                });
        builder.show();
    }


    public void onCreditsClick( View source ) {
        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( R.string.main_menu_credits_title )
               .setMessage( R.string.main_menu_credits_message )
               .setCancelable( true );
        builder.show();
    }
}
