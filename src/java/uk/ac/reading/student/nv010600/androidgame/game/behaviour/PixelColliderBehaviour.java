package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;


public class PixelColliderBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public PixelColliderBehaviour( Entity parent ){
        super( parent );
    }


    @Override
    public void initialise() {
        mCollider = parent.getAttribute( ColliderAttributes.class );
        mGraphics = parent.getAttribute( GraphicsAttributes.class );
        mMovement = parent.getAttribute( MovementAttributes.class );
        ColliderAttributes.addWaitingCollider( parent );
    }


    @Override
    public void destroy() {
        ColliderAttributes.removeWaitingCollider( parent );
        ColliderAttributes.removeProcessedCollider( parent );
    }


    @Override
    public void update( float elapsed ) {

        // reset updated collider lists, if necessary
        if( ColliderAttributes.waitingColliders().isEmpty() ){
            ColliderAttributes.setWaitingColliders( new HashSet<>( ColliderAttributes.processedColliders() ) );
            ColliderAttributes.clearProcessedColliders();    // does not resize the list, keeps allocated size
        }


        // swap, mark as having been compared
        if( ColliderAttributes.removeWaitingCollider( parent ) ){
            ColliderAttributes.addProcessedCollider( parent );
        }


        // detect collisions
        for( Entity other : ColliderAttributes.waitingColliders() ){
            if( other == parent || !other.isValid() ) continue; // check if self ( here == performs instance/reference check )

            // check if in same group, if so do not compare
            ColliderAttributes otherCollider = other.getAttribute( ColliderAttributes.class );
            if( other.getAttribute( ColliderAttributes.class ).group() == mCollider.group() ) continue;


            // detect collision
            GraphicsAttributes otherGraphics = other.getAttribute( GraphicsAttributes.class );
            MovementAttributes otherMovement = other.getAttribute( MovementAttributes.class );

            if( detectCollision( mGraphics.graphic(), mMovement.position(), otherGraphics.graphic(), otherMovement.position() ) ){
                if( mCollider.listeners() != null ){
                    for( ColliderAttributes.Listener listener : mCollider.listeners() ) listener.onCollision( other );
                }

                if( otherCollider.listeners() != null ){
                    for( ColliderAttributes.Listener listener : otherCollider.listeners() ) listener.onCollision( parent );
                }
            }
        }
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient ColliderAttributes mCollider;
    private transient GraphicsAttributes mGraphics;
    private transient MovementAttributes mMovement;


    private boolean detectCollision( Bitmap b1, Vector2 p1, Bitmap b2, Vector2 p2 ) {
        //if( Vector2.distance( p1, p2 ) > Math.max( b1.getWidth(), b1.getHeight() ) + Math.max( b2.getWidth(), b2.getHeight() )/2 ) return false;

        if( b1 == null || b2 == null ) return false;

        Rect bounds1 = new Rect( (int)p1.x - b1.getWidth()/2, (int)p1.y - b1.getHeight()/2,
                                 (int)p1.x + b1.getWidth()/2, (int)p1.y + b1.getHeight()/2 );
        Rect bounds2 = new Rect( (int)p2.x - b2.getWidth()/2, (int)p2.y - b2.getHeight()/2,
                                 (int)p2.x + b2.getWidth()/2, (int)p2.y + b2.getHeight()/2 );

        if( bounds1.intersect( bounds2 ) ){
            return true;
            /*Rect collision = collisionBounds( bounds1, bounds2 );

            for( int x = collision.left; x < collision.right; x++ ){
                for( int y = collision.top; y < collision.bottom; y++ ){
                    int pixel1 = b1.getPixel( x - bounds1.left, y - bounds1.top );
                    int pixel2 = b2.getPixel( x - bounds2.left, y - bounds2.top );

                    if( pixel1 != ColliderAttributes.COLOUR_KEY && pixel2 != ColliderAttributes.COLOUR_KEY ){
                        return true;
                    }
                }
            }*/
        }

        return false;
    }


    private Rect collisionBounds( Rect b1, Rect b2 ) {
        return new Rect(
                Math.max( b1.left, b2.left ),
                Math.max( b1.top, b2.top ),
                Math.min( b1.right, b2.right ),
                Math.min( b1.bottom, b2.bottom )
        );
    }
}
