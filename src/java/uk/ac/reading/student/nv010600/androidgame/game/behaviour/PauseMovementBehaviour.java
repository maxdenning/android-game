package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.PauseMovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;


public class PauseMovementBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public PauseMovementBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mPause = parent.getAttribute( PauseMovementAttributes.class );
        mMovement = parent.getAttribute( MovementAttributes.class );
    }


    @Override
    public void update( float elapsed ) {
        if( mPause.timer() > mPause.delay() + mPause.pauseLength() ) return;
        mPause.setTimer( mPause.timer() + elapsed );


        // Wait until pre-delay is complete
        if( mPause.timer() < mPause.delay() ) return;


        if( mPause.timer() > mPause.delay() + mPause.pauseLength() ){
            mMovement.setIsMoving( true );      // Switch back
        }else{
            mMovement.setIsMoving( false );     // Stop movement for length
        }
    }


    // ----- Private ---------------------------------------------------------------------------- //
    private transient PauseMovementAttributes mPause;
    private transient MovementAttributes mMovement;
}
