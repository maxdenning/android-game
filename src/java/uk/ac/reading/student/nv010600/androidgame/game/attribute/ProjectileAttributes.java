package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;


public class ProjectileAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public ProjectileAttributes() {
        this( ProjectileType.LASER );
    }


    public ProjectileAttributes( ProjectileType type ) {
        setType( type );
    }


    public enum ProjectileType { LASER, MISSILE }



    // ----- Private ---------------------------------------------------------------------------- //
    private ProjectileType mType;                               // type of projectile to emit


    // ----- Public Accessors ------------------------------------------------------------------- //
    public ProjectileType type() { return mType; }
    public void setType( ProjectileType type ) { mType = type; }
}
