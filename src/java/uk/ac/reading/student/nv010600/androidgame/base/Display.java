package uk.ac.reading.student.nv010600.androidgame.base;
import android.content.res.Resources;


/**
 * Contains properties relevant to the display of the game/device
 *
 * @author Max Denning
 */
public class Display {

    /**
     * Converts density pixels to pixels
     * @param dp    density pixel value to convert
     * @return      converted value in pixels
     */
    public static float dp2px( float dp ) {
        return dp * Resources.getSystem().getDisplayMetrics().density;
    }


    /**
     * Converts pixels to density pixels
     * @param px    pixel value to convert
     * @return      converted value in density pixels
     */
    public static float px2dp( float px ) {
        return px / Resources.getSystem().getDisplayMetrics().density;
    }


    /**
     * Converts vector of density pixels to pixels
     * @param dp    vector of values to convert
     * @return      converted values in pixels
     */
    public static Vector2 dp2px( Vector2 dp ) {
        return new Vector2( dp2px( dp.x ), dp2px( dp.y ) );
    }


    /**
     * Converts vector of pixels to density pixels
     * @param dp    vector of values to convert
     * @return      converted values in density pixels
     */
    public static Vector2 px2dp( Vector2 dp ) {
        return new Vector2( px2dp( dp.x ), px2dp( dp.y ) );
    }


    /**
     * @return width of the display in pixels
     */
    public static int widthPx() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }


    /**
     * @return height of the display in pixels
     */
    public static int heightPx() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    /**
     * @return width of the display in density pixels
     */
    public static float widthDp() {
        return px2dp( widthPx() );
    }


    /**
     * @return height of the display in density pixels
     */
    public static float heightDp() {
        return px2dp( heightPx() );
    }
}
