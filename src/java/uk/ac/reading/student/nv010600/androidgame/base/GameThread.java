package uk.ac.reading.student.nv010600.androidgame.base;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;


public abstract class GameThread extends Thread implements View.OnTouchListener {

    // ----- Public ----------------------------------------------------------------------------- //

    //Different mMode states
    public static final int STATE_LOSE = 1;
    public static final int STATE_PAUSE = 2;
    public static final int STATE_READY = 3;
    public static final int STATE_RUNNING = 4;
    public static final int STATE_WIN = 5;




    public GameThread( Context context, SurfaceHolder holder, Handler handler ) {
        mContext = context;
        mSurfaceHolder = holder;
        mHandler = handler;
        mLastTouchTime = System.currentTimeMillis();
    }


    /*
     * Called when app is destroyed, so not really that important here
     * But if (later) the game involves more thread, we might need to stop a thread, and then we would need this
     * Dare I say memory leak...
     */
    public void cleanup() {
        mContext = null;
        mHandler = null;
        mSurfaceHolder = null;

        try{
            if( mDrawThread != null ) mDrawThread.join();
        }catch( InterruptedException e ) {
            Log.d( "GT", e.getMessage() );
        }
    }


    @Override
    public void run() {

        /*Canvas canvas;

        while( mRunning ){
            canvas = null;
            try{
                canvas = mSurfaceHolder.lockCanvas( null );
                synchronized( mMonitor ){
                    if( mMode == STATE_RUNNING ) updateGame();
                    draw( canvas );
                }

            }finally{
                if( canvas != null && mSurfaceHolder != null ) mSurfaceHolder.unlockCanvasAndPost( canvas );
            }


            synchronized( mMonitor ){
                if( mMode == STATE_RUNNING ) updateGame();
            }
        }*/

        mDrawThread = new DrawThread( this, mSurfaceHolder );
        mDrawThread.start();


        while( mRunning ){
            synchronized( mMonitor ){
                if( mMode == STATE_RUNNING ) updateGame();
            }
        }

        try{
            if( mDrawThread != null ) mDrawThread.join();
        }catch( InterruptedException e ) {
            Log.d( "GT", e.getMessage() );
        }
    }


    private void updateGame() {
        mNow = System.currentTimeMillis();
        if( mLastTouchTime > 0 && (mNow - mLastTouchTime) / 1000.0f > PAUSE_TIMER_MAX ) setState( STATE_PAUSE );

        update( (mNow - mLastTime) / 1000.0f );
        mLastTime = mNow;
    }


    @Override
    public boolean onTouch( View v, MotionEvent e ){
        // Sent via GameView

        mLastTouchTime = 0;

        if( e.getAction() == MotionEvent.ACTION_DOWN && mMode == STATE_READY ){
            //begin();
            setState( STATE_RUNNING );
            Log.d( "GameThread", "READY DOWN  -->  RUNNING" );
            return true;
        }

        if( e.getAction() == MotionEvent.ACTION_DOWN && mMode == STATE_PAUSE ){
            //unpause();
            setState( STATE_RUNNING );
            Log.d( "GameThread", "PAUSE DOWN  -->  RUNNING" );
            return true;
        }

        if( e.getAction() == MotionEvent.ACTION_UP && mMode == STATE_RUNNING ){
            mLastTouchTime = mNow;
            Log.d( "GameThread", "RUNNING UP  -->  PAUSE" );
            return true;
        }

        return false;
    }




    public void setSurfaceSize( int width, int height ) {
        synchronized( mMonitor ){
            mCanvasWidth = width;
            mCanvasHeight = height;
        }
    }


    public void setState( int mode ) {
        //Send messages to View/Activity thread
        synchronized( mMonitor ){
            setState( mode, null );
        }
    }


    public void setState( int mode, CharSequence message ) {
        if( mMode == mode ) return;

        Message msg = mHandler.obtainMessage();
        Bundle b = new Bundle();
        CharSequence str = "";

        synchronized( mMonitor ){
            //mMode = mode;

            switch( mode ){
                case STATE_READY:
                    break;

                case STATE_RUNNING:
                    if( mMode == STATE_READY ){
                        // begin
                        initialise();
                        mLastTime = System.currentTimeMillis() + 100;

                    }else if( mMode == STATE_PAUSE ){
                        // unpause
                        mLastTime = System.currentTimeMillis();
                    }
                break;

                case STATE_PAUSE:
                    if( mMode != STATE_RUNNING ) return; // only pause from running
                    break;

                case STATE_LOSE:
                    break;

                case STATE_WIN:
                    break;

                default: break;
            }

            mMode = mode;

            if( message != null ) str = message + "\n" + str;
            b.putString( "text", str.toString() );
            msg.setData( b );
            mHandler.sendMessage( msg );
        }

        /*
        if( mMode == STATE_RUNNING ){
            Message msg = mHandler.obtainMessage();
            Bundle b = new Bundle();
            //b.putString( "text", "" );
            //b.putInt( "viz", View.INVISIBLE );
            //b.putBoolean( "showAd", false );
            msg.setData( b );
            mHandler.sendMessage( msg );

        }else{
            Message msg = mHandler.obtainMessage();
            Bundle b = new Bundle();

            //Resources res = mContext.getResources();
            CharSequence str = "";
            if( mMode == STATE_READY ){
                //str = res.getText(R.string.mode_ready);


            }else if( mMode == STATE_PAUSE ){
                //str = res.getText(R.string.mode_pause);

            }else if( mMode == STATE_LOSE ){
                //str = res.getText(R.string.mode_lose);

            }else if( mMode == STATE_WIN ){
                //str = res.getText(R.string.mode_win);

            }

            if( message != null ) str = message + "\n" + str;
            b.putString( "text", str.toString() );
            //b.putInt( "viz", View.VISIBLE );

            msg.setData( b );
            mHandler.sendMessage( msg );
        }
        */
    }



    // ----- Protected -------------------------------------------------------------------------- //
    protected int mCanvasWidth = 1;
    protected int mCanvasHeight = 1;
    protected Context mContext;                         //Android Context - this stores almost all we need to know

    protected abstract void initialise();               //Pre-begin a game
    protected abstract void update( float elapsed );    // param: elapsed in seconds
    protected abstract void draw( Canvas canvas );



    // ----- Private ---------------------------------------------------------------------------- //
    private boolean mRunning = false;               //Control of the actual running inside run()
    private SurfaceHolder mSurfaceHolder;           //The surface this thread (and only this thread) writes upon
    private Handler mHandler;                       //the message handler to the View/Activity thread
    private static final Integer mMonitor = 1;      //Used to ensure appropriate threading
    private int mMode = 1;                          //Control variable for the mode of the game (e.g. STATE_WIN)


    //Used for time keeping
    private long mNow;
    private long mLastTime = 0;                     //Last time we updated the game physics

    private long mLastTouchTime;
    private final float PAUSE_TIMER_MAX = 6;

    private DrawThread mDrawThread;



    //Starting up the game
    /*public void begin() {
        synchronized( mMonitor ) {
            initialise();
            mLastTime = System.currentTimeMillis() + 100;
            setState( STATE_RUNNING );
        }
    }


    public void pause() {
        synchronized( mMonitor ){
            if( mMode == STATE_RUNNING ) setState( STATE_PAUSE );
        }
    }


    public void unpause() {
        // Move the real time clock up to now
        synchronized( mMonitor ){
            mLastTime = System.currentTimeMillis();
        }
        setState( STATE_RUNNING );
    }*/





    // ----- Public Accessors ------------------------------------------------------------------- //
    public boolean isRunning() { return mRunning; }
    public int getMode() { return mMode; }
    public Context getContext() { return mContext; }

    public void setSurfaceHolder( SurfaceHolder h ) { mSurfaceHolder = h; }
    public void setRunning( boolean running ) { mRunning = running; }
}
