package uk.ac.reading.student.nv010600.androidgame.game.upgrade;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;


public abstract class Upgrade {

    // ----- Public ----------------------------------------------------------------------------- //
    public enum State { WAITING, RUNNING, COMPLETE };


    public Upgrade( int graphicResourceID ) {
        parent = null;
        mState = State.WAITING;
        mGraphicResourceID = graphicResourceID;
    }


    public void begin( Entity parent ) {
        this.parent = parent;
        mState = State.RUNNING;
    }


    public void update( float elapsed ) {

    }



    // ----- Protected -------------------------------------------------------------------------- //
    protected Entity parent;

    protected void markCompleted() {
        mState = State.COMPLETE;
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private State mState;
    private int mGraphicResourceID;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public State state() { return mState; }
    public int graphicResource() { return mGraphicResourceID; }
}
