package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import android.util.Log;

import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Display;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;


public class BasicMovementBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public BasicMovementBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mMovement = parent.getAttribute( MovementAttributes.class );

        mEdgeBound = Display.dp2px( 5 );
        if( parent.hasAttribute( GraphicsAttributes.class ) ){
            Vector2 size = parent.getAttribute( GraphicsAttributes.class ).size();
            mEdgeBound = Display.dp2px( Math.max( size.x, size.y ) );
        }
    }


    @Override
    public void update( float elapsed ) {
        if( !mMovement.isMoving() ){
            mMovement.setSpeed( 0 );
            return;
        }


        if( mMovement.acceleration() == -1 ) mMovement.setSpeed( mMovement.maxSpeed() );
        else if( mMovement.speed() < mMovement.maxSpeed() ) mMovement.setSpeed( mMovement.speed() + (mMovement.acceleration() * elapsed) );

        mMovement.setPosition( mMovement.position().x + Display.dp2px( mMovement.direction().x * mMovement.speed() * elapsed ),
                               mMovement.position().y + Display.dp2px( mMovement.direction().y * mMovement.speed() * elapsed ) );


        /*if( mMovement.position().x < 0 || mMovement.position().x > Display.widthPx()
         || mMovement.position().y < -mEdgeBound || mMovement.position().y > Display.heightPx() ){
            //Log.d( "BMB", String.format( "%f %f %f", mEdgeBound, mMovement.position().x, mMovement.position().y ) );
            parent.destroy();
        }*/

        if( mMovement.position().x < -mEdgeBound || mMovement.position().x > Display.widthPx() + mEdgeBound
         || mMovement.position().y < -mEdgeBound || mMovement.position().y > Display.heightPx() + mEdgeBound ){
            //Log.d( "BMB", String.format( "%f %f %f", mEdgeBound, mMovement.position().x, mMovement.position().y ) );
            parent.destroy();
        }
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient MovementAttributes mMovement;
    private transient float mEdgeBound;
}
