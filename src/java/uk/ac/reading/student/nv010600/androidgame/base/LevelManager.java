package uk.ac.reading.student.nv010600.androidgame.base;
import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;


/**
 *
 * Create level builder (either separately or as part of this class)
 * Use this to create level managers which are then serialised
 *
 *
 */

public class LevelManager implements Serializable {

    // ----- Public ----------------------------------------------------------------------------- //
    public LevelManager() {
        this( null );
    }


    public LevelManager( Context context ) {
        mEntityQueue = new TreeMap<>();
        mTimer = 0;
        mContext = context;
    }


    public void update( float elapsed ) {
        if( isComplete() ) return;

        mTimer += elapsed;
        if( mTimer <= 0 ) return;

        SortedMap<Float,List<Entity>> sub = mEntityQueue.subMap( mTimeOffset + mTimer - elapsed, mTimeOffset + mTimer );
        if( sub.size() <= 0 ) return;

        for( List<Entity> entities : sub.values() ){
            for( Entity entity : entities ){
                Entity.launch( entity, mContext );
                //Log.d( "LevelManager", "Launched queued entity" );
            }
        }
    }



    // ----- Protected -------------------------------------------------------------------------- //
    protected TreeMap<Float, List<Entity>> mEntityQueue;       // < inclusion time, list of entities to spawn at this time >
    protected transient Context mContext;



    // ----- Private ---------------------------------------------------------------------------- //
    private transient float mTimer;
    private transient final float mTimeOffset = 0;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public final TreeMap<Float, List<Entity>> queue() { return mEntityQueue; }
    public final boolean isComplete() { return mEntityQueue.lastKey() <= mTimer && Entity.entityCount() <= 1; }

    public final void setContext( Context context ) { mContext = context; }
    public final void resetTimer() { mTimer = 0; }
}
