package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.TouchControlAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.entities.PlayerEntity;
import uk.ac.reading.student.nv010600.androidgame.game.entities.ProjectileEntity;

import android.util.Log;
import android.view.MotionEvent;


public class ProjectileGeneratorBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public ProjectileGeneratorBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mInput = parent.getAttribute( TouchControlAttributes.class );
        mProjectile = parent.getAttribute( ProjectileGeneratorAttributes.class );
        mMovement = parent.getAttribute( MovementAttributes.class );
        mResetLatch = false;
    }


    @Override
    public void update( float elapsed ) {

        boolean reset = false;
        if( mInput != null ){
            reset = mInput.lastEvent() == null || (mInput.lastEvent().getAction() != MotionEvent.ACTION_DOWN && mInput.lastEvent().getAction() != MotionEvent.ACTION_MOVE);
            if( mResetLatch && reset ) return;
            mResetLatch = reset;
        }


        for( ProjectileGeneratorAttributes.Generator generator : mProjectile.generators() ){
            if( reset ){
                generator.setGroupCounter( 0 );
                continue;
            }

            // Check if waiting for group
            if( generator.groupDelayTimer() >= 0 && generator.groupDelayTimer() <= generator.groupDelay() ){
                generator.setGroupDelayTimer( generator.groupDelayTimer() + elapsed );
                continue;
            }

            // Check if waiting for projectile
            if( generator.delayTimer() >= 0 && generator.delayTimer() <= generator.delay() ){
                generator.setDelayTimer( generator.delayTimer() + elapsed );
                continue;
            }


            // Generate projectiles
            //Log.d( "ProjectileGenerator", String.format( "EMIT %d", generator.groupCounter() ) );



            // calculate spawn point / spawn direction, depending on generator.type
            Vector2 spawnPos = Vector2.add( mMovement.position(), Display.dp2px( generator.source() ) );

            float separation = (generator.streams() <= 1) ? generator.spread() : generator.spread()/(generator.streams()-1);

            int ydirmod = (parent.hasAttribute( PlayerEntity.PlayerAttribute.class)) ? -1 : 1;
            int collisionGroup = (parent.hasAttribute( ColliderAttributes.class )) ? parent.getAttribute( ColliderAttributes.class ).group() : ColliderAttributes.DEFAULT_GROUP;

            for( int i = 0; i < generator.streams(); i++ ){
                Entity projectile = Entity.launch( new ProjectileEntity( parent.context(), spawnPos, generator.type(), collisionGroup ) );
                MovementAttributes projmov = projectile.getAttribute( MovementAttributes.class );

                switch( generator.pattern() ){
                    case PARALLEL:
                        // spawn with an x offset from source
                        projmov.setPosition( projmov.position().x + Display.dp2px( (separation*i) - (generator.spread()/2) ),
                                             projmov.position().y );
                        break;

                    case RADIAL:
                        // spawn at source with angular direction
                        projmov.setDirection( Vector2.normalise( ((180-generator.spread())/2 + (separation*i))*(3.1415f/180) ) );
                        break;

                    default:
                        Log.d( "PGB", "Unrecognised generator pattern!" );
                        break;
                }

                projmov.setDirection( projmov.direction().x, projmov.direction().y * ydirmod );
            }


            // Increment group counter & end group or more to next projectile
            generator.setGroupCounter( generator.groupCounter() + 1 );

            if( generator.groupCounter() == 0 ){
                generator.setGroupDelayTimer( 0 );
                generator.setDelayTimer( -1 );
            }else{
                generator.setGroupDelayTimer( -1 );
                generator.setDelayTimer( 0 );
            }
        }

    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient TouchControlAttributes mInput;
    private transient ProjectileGeneratorAttributes mProjectile;
    private transient MovementAttributes mMovement;
    private transient boolean mResetLatch;
}
