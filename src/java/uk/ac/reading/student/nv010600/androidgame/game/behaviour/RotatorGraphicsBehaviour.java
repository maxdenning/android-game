package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import android.graphics.Canvas;
import android.graphics.Matrix;


public class RotatorGraphicsBehaviour extends BasicGraphicsBehaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public RotatorGraphicsBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        super.initialise();
        mMovement = parent.getAttribute( MovementAttributes.class );
        mGraphics = parent.getAttribute( GraphicsAttributes.class );
    }


    @Override
    public void draw( Canvas canvas ){
        if( !parent.isValid() || !mGraphics.visible() ) return;

        Matrix mat = new Matrix();
        mat.postRotate( mMovement.direction().degrees() - 90 );
        mat.postTranslate( mMovement.position().x, mMovement.position().y );
        canvas.drawBitmap( mGraphics.graphic(), mat, null );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient MovementAttributes mMovement;
    private transient GraphicsAttributes mGraphics;
}
