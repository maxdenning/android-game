package uk.ac.reading.student.nv010600.androidgame.base;
import java.io.Serializable;


/**
 * Provides abstract interface through which behaviours can act on Attributes as part of the ECS
 * pattern.
 * Must not contain properties relevant to the parent entity, these should be in an Attribute.
 *
 * @author Max Denning
 */
public abstract class Behaviour implements Serializable {

    // ----- Public ----------------------------------------------------------------------------- //

    /**
     * Set the parent of the behaviour
     * @param parent    parent of behaviour
     */
    public Behaviour( Entity parent ) {
        this.parent = parent;
    }


    /**
     * Called after construction and before update, this is where variables should be initialised
     * and where attribute dependencies defined.
     */
    public abstract void initialise();


    /**
     * Called before behaviour is removed from parent entity
     */
    public void destroy(){};


    /**
     * Where the effect the behaviour has on the parent should be defined
     * @param elapsed   time elapsed since last update call
     */
    public abstract void update( float elapsed );



    // ----- Protected -------------------------------------------------------------------------- //
    /** Parent of behaviour */
    protected Entity parent;
}
