package uk.ac.reading.student.nv010600.androidgame.game.entities;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.InitRes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.DamageAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ScoreAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicGraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicMovementBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.DamageBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.PixelColliderBehaviour;
import android.content.Context;


public class MeteorEntity extends Entity {

    public MeteorEntity( Context context, float maxSpeed, Vector2 direction ) {
        super( context );
        InitRes i = new InitRes( context );

        addAttribute( new GraphicsAttributes( R.drawable.meteor, i.getVector2( R.string.init_meteor_graphics_size ) ) );
        addAttribute( new MovementAttributes( new Vector2(), maxSpeed, -1, direction, true ) );
        addAttribute( new ColliderAttributes() );
        addAttribute( new DamageAttributes( 25 ) );
        addAttribute( new HealthAttributes( 30 ) );
        addAttribute( new ScoreAttributes( 100 ) );

        addBehaviour( new BasicGraphicsBehaviour( this ) );
        addBehaviour( new BasicMovementBehaviour( this ) );
        addBehaviour( new PixelColliderBehaviour( this ) );
        addBehaviour( new DamageBehaviour( this ) );
    }

}
