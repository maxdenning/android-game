package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import java.util.ArrayList;

import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.UpgradeReceiverAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.upgrade.Upgrade;


public class UpgradeReceiverBehaviour extends Behaviour{

    // ----- Public ----------------------------------------------------------------------------- //
    public UpgradeReceiverBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mUpgrades = parent.getAttribute( UpgradeReceiverAttributes.class );
    }


    @Override
    public void update( float elapsed ) {
        if( mUpgrades.upgrades().isEmpty() ) return;

        ArrayList<Upgrade> completed = new ArrayList<>();

        for( Upgrade upgrade : mUpgrades.upgrades() ){
            // Start upgrade effect
            if( upgrade.state() == Upgrade.State.WAITING ){
                upgrade.begin( parent );
                continue;
            }

            // Update upgrade & remove those that are complete
            upgrade.update( elapsed );
            if( upgrade.state() == Upgrade.State.COMPLETE ) completed.add( upgrade );
        }

        for( Upgrade upgrade : completed ) mUpgrades.removeUpgrade( upgrade );
    }


    // ----- Private ---------------------------------------------------------------------------- //
    private transient UpgradeReceiverAttributes mUpgrades;
}
