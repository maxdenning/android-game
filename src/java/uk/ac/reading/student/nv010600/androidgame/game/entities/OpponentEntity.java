package uk.ac.reading.student.nv010600.androidgame.game.entities;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.InitRes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.DamageAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.PauseMovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileAttributes.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ScoreAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SeekerAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SinusoidControlAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicGraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicMovementBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.DamageBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.PauseMovementBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.PixelColliderBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.ProjectileGeneratorBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.RandomControlBehvaiour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.RotatorGraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.SeekerControlBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.SinusoidControlBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.UpgradeDropBehaviour;
import android.content.Context;

import java.util.ArrayList;


public class OpponentEntity extends Entity {

    public enum OpponentType { BASIC_1, BASIC_2, BASIC_3,
                               RAM_1, RAM_2, RAM_3,
                               ATTACK_1, ATTACK_2, ATTACK_3,
                               BOSS_1
    }


    public OpponentEntity( Context context, float maxSpeed, float acceleration, int hitpoints, int damage, int score ) {
        super( context );

        // Attributes
        addAttribute( new MovementAttributes( new Vector2(), maxSpeed, acceleration, new Vector2( 0, 1 ), true ) );
        addAttribute( new HealthAttributes( hitpoints ) );
        addAttribute( new DamageAttributes( damage ) );
        addAttribute( new ScoreAttributes( score ) );
        addAttribute( new ColliderAttributes( ) );//ColliderAttributes.DEFAULT_GROUP, null ) );


        // Behaviours
        addBehaviour( new BasicMovementBehaviour( this ) );
        addBehaviour( new DamageBehaviour( this ) );
        addBehaviour( new PixelColliderBehaviour( this ) );
        addBehaviour( new BasicGraphicsBehaviour( this ) );
        addBehaviour( new UpgradeDropBehaviour( this ) );
    }


    public OpponentEntity( Context context, OpponentType type ) {
        super( context );
        InitRes i = new InitRes( context );

        // Common features
        addAttribute( new ColliderAttributes() );
        addBehaviour( new PixelColliderBehaviour( this ) );
        addBehaviour( new BasicMovementBehaviour( this ) );
        addBehaviour( new DamageBehaviour( this ) );
        addBehaviour( new UpgradeDropBehaviour( this ) );



        // Specific unit behaviours/types
        int damage = 0, resID = R.drawable.placeholder, hitpoints = 1, score = 0;
        Vector2 size = i.getVector2( R.string.init_placeholder_graphics_size );
        float maxSpeed = 1;
        ArrayList<Generator> generators = new ArrayList<>();

        switch( type ){

            // ----- BASIC ---------------------------------------------------------------------- //
            case BASIC_2:   // Basic, linear laser shooter, with wait
                addAttribute( new PauseMovementAttributes( 1.0f, 2.0f ) );
                addBehaviour( new PauseMovementBehaviour( this ) );


            case BASIC_1:    // Basic, linear laser shooter
                damage = 20;
                resID = R.drawable.opponent3;
                size = new Vector2( i.getVector2( R.string.init_opponent3_graphics_size ) );
                hitpoints = 30;
                maxSpeed = 45;
                score = 75;
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.LASER, 0.6f, 1.0f, 1, 1, Pattern.PARALLEL, 0, new Vector2(0, size.y/4) ) );

                addBehaviour( new BasicGraphicsBehaviour( this ) );
                break;


            case BASIC_3:    // Basic, linear laser shooter, with wait and sinusoid
                damage = 20;
                resID = R.drawable.opponent3;
                size = new Vector2( i.getVector2( R.string.init_opponent3_graphics_size ) );
                hitpoints = 30;
                maxSpeed = 45;
                score = 75;
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.LASER, 0.6f, 1.0f, 1, 1, Pattern.PARALLEL, 0, new Vector2(0, size.y/4) ) );

                addBehaviour( new BasicGraphicsBehaviour( this ) );

                addAttribute( new SinusoidControlAttributes( 10, 10 ) );
                addBehaviour( new SinusoidControlBehaviour( this ) );
                break;


            // ----- RAM ------------------------------------------------------------------------ //
            case RAM_1:     // Ram, no laser, no control
                damage = 75;
                resID = R.drawable.opponent1;
                size = new Vector2( i.getVector2( R.string.init_opponent1_graphics_size ) );
                hitpoints = 40;
                maxSpeed = 250;
                score = 100;

                addBehaviour( new BasicGraphicsBehaviour( this ) );
                break;


            case RAM_2:     // Ram, no laser, wait and seek control
                damage = 50;
                resID = R.drawable.opponent1;
                size = new Vector2( i.getVector2( R.string.init_opponent1_graphics_size ) );
                hitpoints = 40;
                maxSpeed = 250;
                score = 200;

                addAttribute( new PauseMovementAttributes( 0.25f, 2.0f ) );
                addBehaviour( new PauseMovementBehaviour( this ) );

                addAttribute( new SeekerAttributes( 0.1f, 0.5f, 1000, 3 ) );
                addBehaviour( new SeekerControlBehaviour( this ) );

                addBehaviour( new RotatorGraphicsBehaviour( this ) );
                break;


            case RAM_3:     // Ram, no laser, wait and random control
                damage = 75;
                resID = R.drawable.opponent1;
                size = new Vector2( i.getVector2( R.string.init_opponent1_graphics_size ) );
                hitpoints = 40;
                maxSpeed = 250;
                score = 80;

                addAttribute( new PauseMovementAttributes( 0.25f, 1.5f ) );
                addBehaviour( new PauseMovementBehaviour( this ) );

                addBehaviour( new RandomControlBehvaiour( this ) );

                addBehaviour( new RotatorGraphicsBehaviour( this ) );
                break;


            // ----- ATTACK --------------------------------------------------------------------- //
            case ATTACK_1:      // Attack, slow, multi-laser
                damage = 25;
                resID = R.drawable.opponent2;
                size = new Vector2( i.getVector2( R.string.init_opponent2_graphics_size ) );
                hitpoints = 90;
                maxSpeed = 75;
                score = 120;
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.LASER, 0.3f, 1.0f, 2, 2, Pattern.PARALLEL, 32, new Vector2(0, size.y/4) ) );

                addBehaviour( new BasicGraphicsBehaviour( this ) );
                break;


            case ATTACK_2:      // Attack, slow, missile
                damage = 25;
                resID = R.drawable.opponent2;
                size = new Vector2( i.getVector2( R.string.init_opponent2_graphics_size ) );
                hitpoints = 90;
                maxSpeed = 75;
                score = 140;
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.MISSILE, 0.5f, 0.5f, 1, 1, Pattern.PARALLEL, 0, new Vector2(0, size.y/4) ) );

                addBehaviour( new BasicGraphicsBehaviour( this ) );
                break;


            case ATTACK_3:      // Attack, slow, radial lasers
                damage = 25;
                resID = R.drawable.opponent2;
                size = new Vector2( i.getVector2( R.string.init_opponent2_graphics_size ) );
                hitpoints = 90;
                maxSpeed = 75;
                score = 140;
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.LASER, 0.3f, 0.3f, 2, 3, Pattern.RADIAL, 120, new Vector2(0, size.y/4) ) );

                addBehaviour( new BasicGraphicsBehaviour( this ) );
                break;


            // ----- BOSS ----------------------------------------------------------------------- //
            case BOSS_1:        // Boss, slow, waits, parrallel + radial lasers + missiles
                damage = 25;
                resID = R.drawable.opponent4;
                size = new Vector2( i.getVector2( R.string.init_opponent4_graphics_size ) );
                hitpoints = 2000;
                maxSpeed = 100;
                score = 850;
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.LASER, 0.4f, 0.75f, 4, 4, Pattern.PARALLEL, 128, new Vector2(0, size.y/4) ) );
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.LASER, 0.1f, 1.0f, 1, 6, Pattern.RADIAL, 120, new Vector2(0, size.y/4) ) );
                generators.add( new ProjectileGeneratorAttributes().new Generator( ProjectileType.MISSILE, 0.1f, 1.2f, 1, 3, Pattern.RADIAL, 180, new Vector2(0, size.y/4) ) );

                addAttribute( new PauseMovementAttributes( 0.75f, 100f ) );
                addBehaviour( new PauseMovementBehaviour( this ) );

                addBehaviour( new BasicGraphicsBehaviour( this ) );
                break;



            default: break;
        }

        //ProjectileType type, float delay, float groupDelay, int groupSize, int streams, Pattern pattern, float spread, Vector2 source


        addAttribute( new DamageAttributes( damage ) );
        addAttribute( new GraphicsAttributes( resID, size ) );
        addAttribute( new HealthAttributes( hitpoints ) );
        addAttribute( new MovementAttributes( new Vector2(), maxSpeed, -1, new Vector2( 0, 1 ), true ) );
        addAttribute( new ScoreAttributes( score ) );

        if( !generators.isEmpty() ){
            addAttribute( new ProjectileGeneratorAttributes( generators ) );
            addBehaviour( new ProjectileGeneratorBehaviour( this ) );
        }
    }


}
