package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import android.util.Log;

import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SinusoidControlAttributes;

public class SinusoidControlBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public SinusoidControlBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mSinusoid = parent.getAttribute( SinusoidControlAttributes.class );
        mMovement = parent.getAttribute( MovementAttributes.class );
    }


    @Override
    public void update( float elapsed ) {
        if( !mMovement.isMoving() ) return;

        mSinusoid.setTimer( mSinusoid.timer() + elapsed );

        float amp = mSinusoid.amplitude() * elapsed;
        float fx = amp * (float)Math.sin( mSinusoid.timer() * mSinusoid.frequency() );

        mMovement.setDirection( fx/amp, mMovement.direction().y );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient SinusoidControlAttributes mSinusoid;
    private transient MovementAttributes mMovement;
}
