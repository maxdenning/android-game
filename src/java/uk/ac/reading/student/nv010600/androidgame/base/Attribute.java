package uk.ac.reading.student.nv010600.androidgame.base;
import java.io.Serializable;

/**
 * Simple container for properties as part of the ECS pattern.
 * Must not contain non-getter/setter methods.
 *
 * @author Max Denning
 */
public abstract class Attribute implements Serializable {

    // ----- Public ----------------------------------------------------------------------------- //
    public Attribute() {
    }

}
