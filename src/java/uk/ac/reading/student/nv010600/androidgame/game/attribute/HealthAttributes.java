package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;

/**
 * Contains properties relevant to the current and total hit points of the parent entity.
 * Thread safe.
 *
 * @author Max Denning
 */
public class HealthAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    /**
     * Default constructor, sets max hit-points to 1
     */
    public HealthAttributes() {
        this( 1 );
    }


    /**
     * Constructor, sets max hit-points
     * @param maxHitPoints value of maximum hitpoints
     */
    public HealthAttributes( int maxHitPoints ) {
        setMaxHitPoints( maxHitPoints );
        setHitPoints( maxHitPoints );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    /** Maximum hit-point value, limits mHitPoints */
    private int mMaxHitPoints;

    /** Current hit-point value, limited by mMaxHitPoints */
    private int mHitPoints;


    // ----- Public Accessors ------------------------------------------------------------------- //
    public synchronized int maxHitPoints() { return mMaxHitPoints; }
    public synchronized int hitPoints() { return mHitPoints; }

    public synchronized void setMaxHitPoints( int mhp ) { mMaxHitPoints = Math.max( mhp, 1 ); }
    public synchronized void setHitPoints( int hp ) { mHitPoints = Math.max( 0, Math.min( hp, mMaxHitPoints ) ); }
}
