package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.TouchControlAttributes;

import java.lang.Math;


public class TouchControlBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public TouchControlBehaviour( Entity parent ) {
        super( parent );
    }


    public void initialise() {
        mInput = parent.getAttribute( TouchControlAttributes.class );
        mMovement = parent.getAttribute( MovementAttributes.class );
        mLastEventTime = 0;
        mInput.setDestination( mMovement.position() );
    }


    @Override
    public void update( float elapsed ) {
        if( mInput.lastEvent() != null && mInput.lastEvent().getEventTime() != mLastEventTime ){
            mInput.setDestination( mInput.lastEvent().getRawX(), mInput.lastEvent().getRawY() - Display.dp2px( 60 ) );   // include Y offset so you can see the ship
            mMovement.setIsMoving( true );
            mLastEventTime = mInput.lastEvent().getEventTime();
        }

        if( mMovement.isMoving() && Math.abs( mMovement.position().x - mInput.destination().x ) < Display.dp2px( 8 ) /*mMovement.speed / 100*/
                                 && Math.abs( mMovement.position().y - mInput.destination().y ) < Display.dp2px( 8 ) /*mMovement.speed / 100*/ ) {
            mMovement.setIsMoving( false );
        }

        if( !mMovement.isMoving() ) return;
        mMovement.setDirection( Vector2.sub( mInput.destination(), mMovement.position() ).normalise() );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient TouchControlAttributes mInput;
    private transient MovementAttributes mMovement;
    private transient long mLastEventTime;
}
