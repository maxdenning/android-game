package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import java.util.ArrayList;
import java.util.List;


public class ProjectileGeneratorAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public ProjectileGeneratorAttributes() {
        this( new ArrayList<Generator>() );
    }


    public ProjectileGeneratorAttributes( List<Generator> generators ) {
        setGenerators( generators );
    }


    public enum Pattern { PARALLEL, RADIAL }



    // ----- Private ---------------------------------------------------------------------------- //
    private List<Generator> mGenerators;
    private boolean mIsGenerating;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public List<Generator> generators() { return mGenerators; }

    public void    setGenerators( List<Generator> generators ) { mGenerators = generators; }
    public boolean addGenerator( Generator generator ) { return mGenerators.add( generator ); }
    public boolean removeGenerator( Generator generator ) { return mGenerators.remove( generator ); }



    // ------------------------------------------------------------------------------------------ //
    public class Generator extends ProjectileAttributes {


        // ----- Public ------------------------------------------------------------------------- //
        public Generator() {
            this( ProjectileType.LASER, 0.5f, 2.0f, 3, 2, Pattern.PARALLEL, 20, new Vector2( 0, 0 ) );
        }


        public Generator( ProjectileType type, float delay, float groupDelay, int groupSize, int streams, Pattern pattern, float spread, Vector2 source ){
            super( type );
            setDelay( delay );
            setGroupDelay( groupDelay );
            setGroupSize( groupSize );
            setStreams( streams );
            setPattern( pattern );
            setSpread( spread );
            setSource( source );

            setDelayTimer( -1 );
            setGroupDelayTimer( -1 );
            setGroupCounter( 0 );
        }



        // ----- Private ------------------------------------------------------------------------ //
        private float mDelay;                         // delay between each projectile (seconds)
        private float mDelayTimer;                    // time since last emitted (seconds)

        private float mGroupDelay;                    // delay between each group (seconds)
        private float mGroupDelayTimer;               // time since last group ended (seconds)
        private int mGroupSize;                       // number of projectiles emitted in each group
        private int mGroupCounter;                    // tracks number of projectiles emitted in this group so far (max value = groupSize)

        private int mStreams;                         // number of projectile streams emitted from source point
        private Pattern mPattern;                     // pattern of projectiles produces by source
        private float mSpread;                        // distance (dp) or angle (deg) from furthest left to furthest right

        private Vector2 mSource;                       // source point of projectiles relative to centre of parent (dp)



        // ----- Public Accessors --------------------------------------------------------------- //
        public float   delay() { return mDelay; }
        public float   delayTimer() { return mDelayTimer; }
        public float   groupDelay() { return mGroupDelay; }
        public float   groupDelayTimer() { return mGroupDelayTimer; }
        public int     groupSize() { return mGroupSize; }
        public int     groupCounter() { return mGroupCounter; }
        public int     streams() { return mStreams; }
        public Pattern pattern() { return mPattern; }
        public float   spread() { return mSpread; }
        public Vector2 source() { return mSource; }

        public void setDelay( float delay ) { mDelay = delay; }
        public void setDelayTimer( float delayTimer ) { mDelayTimer = delayTimer; }
        public void setGroupDelay( float groupDelay ) { mGroupDelay = groupDelay; }
        public void setGroupDelayTimer( float groupDelayTimer ) { mGroupDelayTimer = groupDelayTimer; }
        public void setGroupSize( int groupSize ) { mGroupSize = groupSize; }
        public void setGroupCounter( int groupCounter ) { mGroupCounter = groupCounter % groupSize(); }
        public void setStreams( int streams) { mStreams = streams; }
        public void setPattern( Pattern pattern ) { mPattern = pattern; }
        public void setSpread( float spread ) { mSpread = spread; }
        public void setSource( Vector2 source ) { mSource = source.clone(); }
    }
}
