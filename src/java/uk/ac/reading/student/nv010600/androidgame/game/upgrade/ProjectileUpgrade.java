package uk.ac.reading.student.nv010600.androidgame.game.upgrade;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes;


public class ProjectileUpgrade extends Upgrade {

    // ----- Public ----------------------------------------------------------------------------- //
    public ProjectileUpgrade( float duration, ProjectileGeneratorAttributes.Generator generator ) {
        super( R.drawable.upgrade_projectile );
        mDuration = duration;
        mGenerator = generator;
    }


    @Override
    public void begin( Entity parent ) {
        super.begin( parent );

        if( !parent.hasAttribute( ProjectileGeneratorAttributes.class ) ){
            markCompleted();
            return;
        }

        mProjectile = parent.getAttribute( ProjectileGeneratorAttributes.class );
        mProjectile.addGenerator( mGenerator );
    }


    @Override
    public void update( float elapsed ) {
        mTimer += elapsed;
        if( mTimer < mDuration ) return;

        mProjectile.removeGenerator( mGenerator );
        markCompleted();
    }


    // ----- Private ---------------------------------------------------------------------------- //
    private ProjectileGeneratorAttributes.Generator mGenerator;
    private ProjectileGeneratorAttributes mProjectile;
    private float mDuration;
    private float mTimer;
}
