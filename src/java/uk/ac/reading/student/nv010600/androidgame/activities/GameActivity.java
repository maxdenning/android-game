package uk.ac.reading.student.nv010600.androidgame.activities;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.*;
import uk.ac.reading.student.nv010600.androidgame.game.Game;
import uk.ac.reading.student.nv010600.androidgame.game.entities.MeteorEntity;
import uk.ac.reading.student.nv010600.androidgame.game.entities.OpponentEntity;
import uk.ac.reading.student.nv010600.androidgame.game.entities.OpponentEntity.*;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import java.io.IOException;


public class GameActivity extends Activity {

    // ----- Protected -------------------------------------------------------------------------- //
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        requestWindowFeature( Window.FEATURE_NO_TITLE );
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );

        setContentView( R.layout.activity_game );
        mGameView = (GameView)findViewById( R.id.gmv_gamearea );


        LevelBuilder levelBuilder = new LevelBuilder( mGameView.getContext() );
        String levelName = getIntent().getStringExtra( "level" );

        generateLevel1();
        generateLevel2();
        generateLevel3();

        // Generate core levels if not present
        if( !levelBuilder.levelExists( levelName ) ){
            Log.d( "GA", "!" );

            if( levelName.equals( getResources().getString( R.string.level_filename_1 ) ) ) generateLevel1();
            else if( levelName.equals( getResources().getString( R.string.level_filename_2 ) ) ) generateLevel2();
            else if( levelName.equals( getResources().getString( R.string.level_filename_3 ) ) ) generateLevel3();
        }


        try{
            LevelManager level = levelBuilder.load( levelName );
            mGameThread = new Game( mGameView.getContext(), mGameView.getHolder(), mGameView.getHandler(), level );
            mGameView.setThread( mGameThread );
            mGameThread.setState( GameThread.STATE_READY );
            mGameThread.setState( GameThread.STATE_RUNNING );

        }catch( IOException | ClassNotFoundException | IllegalArgumentException e ){
            Toast.makeText( this, "Could not load level", Toast.LENGTH_LONG ).show();
            startActivity( new Intent( getBaseContext(), MainMenuActivity.class ) );
        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        if( mGameThread != null && mGameThread.getMode() == GameThread.STATE_RUNNING ){
            mGameThread.setState( GameThread.STATE_PAUSE );
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if( mGameThread != null ) mGameView.cleanup();
        mGameThread = null;
        mGameView = null;
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private static final int MENU_RESUME = 1;
    private static final int MENU_START = 2;
    private static final int MENU_STOP = 3;

    private GameThread mGameThread;
    private GameView mGameView;







    public void generateLevel1() {
        LevelBuilder builder = new LevelBuilder( getBaseContext() );
        Context ct = getBaseContext();
        float width = Display.widthDp(), height = Display.heightDp();


        builder.buildEntity( 1.0f, width*(3/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 1.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 1.0f, width*(9/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 3.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();
        builder.buildEntity( 3.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();
        builder.buildEntity( 3.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();

        builder.buildEntity( 4.5f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 4.5f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 6.5f, width*(3/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 6.5f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 6.5f, width*(9/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 7.0f, new Vector2( width, height*0.1f ) ).base( new MeteorEntity( ct, 150, new Vector2( -0.75f, 1 ) ) ).create();
        builder.buildEntity( 7.0f, new Vector2( width, height*0.3f ) ).base( new MeteorEntity( ct, 150, new Vector2( -0.75f, 1 ) ) ).create();
        builder.buildEntity( 7.0f, new Vector2( width, height*0.5f ) ).base( new MeteorEntity( ct, 150, new Vector2( -0.75f, 1 ) ) ).create();

        builder.buildEntity( 10.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_3 ) ).create();
        builder.buildEntity( 10.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_3 ) ).create();
        builder.buildEntity( 10.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_3 ) ).create();
        builder.buildEntity( 10.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_3 ) ).create();

        builder.buildEntity( 12.0f, width*(3/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 12.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 12.0f, width*(9/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 15.0f, new Vector2( width, height*0.1f ) ).base( new MeteorEntity( ct, 75, new Vector2( -0.3f, 1 ) ) ).create();
        builder.buildEntity( 15.0f, new Vector2( width, height*0.3f ) ).base( new MeteorEntity( ct, 125, new Vector2( -0.3f, 1 ) ) ).create();
        builder.buildEntity( 15.0f, new Vector2( width, height*0.5f ) ).base( new MeteorEntity( ct, 35, new Vector2( -0.3f, 1 ) ) ).create();

        builder.buildEntity( 18.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_1 ) ).create();
        builder.buildEntity( 18.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_1 ) ).create();

        builder.buildEntity( 20.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 20.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 20.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 22.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_1 ) ).create();
        builder.buildEntity( 22.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_1 ) ).create();
        builder.buildEntity( 22.0f, width*(9/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_1 ) ).create();

        builder.buildEntity( 25.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 25.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 25.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 25.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 27.0f, new Vector2( 0, height*0.1f ) ).base( new MeteorEntity( ct, 75, new Vector2( 0.4f, 1 ) ) ).create();
        builder.buildEntity( 27.0f, new Vector2( 0, height*0.3f ) ).base( new MeteorEntity( ct, 125, new Vector2( 0.2f, 1 ) ) ).create();
        builder.buildEntity( 27.0f, new Vector2( 0, height*0.5f ) ).base( new MeteorEntity( ct, 35, new Vector2( 0.3f, 1 ) ) ).create();


        try{
            builder.saveAs( getResources().getString( R.string.level_filename_1 ) );
        }catch( IOException e ){
            Log.d( "MMA generateLevel1", e.getMessage() );
        }
    }


    public void generateLevel2() {
        LevelBuilder builder = new LevelBuilder( getBaseContext() );
        Context ct = getBaseContext();
        float width = Display.widthDp(), height = Display.heightDp();


        builder.buildEntity( 2.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();

        builder.buildEntity( 4.5f, width*(3/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();
        builder.buildEntity( 4.5f, width*(9/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();

        builder.buildEntity( 6.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 6.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 6.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();

        builder.buildEntity( 8.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 8.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();

        builder.buildEntity( 8.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 8.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 8.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 9.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();
        builder.buildEntity( 9.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();

        builder.buildEntity( 10.5f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();
        builder.buildEntity( 10.5f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();

        builder.buildEntity( 12.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 12.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 12.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 12.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();

        builder.buildEntity( 13.5f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();
        builder.buildEntity( 13.5f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();
        builder.buildEntity( 13.5f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();

        builder.buildEntity( 15.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 15.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 15.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 15.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();

        builder.buildEntity( 17.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 17.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 17.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 17.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();

        builder.buildEntity( 19.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 19.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 19.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();
        builder.buildEntity( 19.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_3 ) ).create();

        builder.buildEntity( 22.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();
        builder.buildEntity( 22.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();

        builder.buildEntity( 23.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();

        builder.buildEntity( 25.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 25.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 25.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 27.0f, width*(3/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 27.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 27.0f, width*(9/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();


        try{
            builder.saveAs( getResources().getString( R.string.level_filename_2 ) );
        }catch( IOException e ){
            Log.d( "MMA generateLevel2", e.getMessage() );
        }
    }


    public void generateLevel3() {
        LevelBuilder builder = new LevelBuilder( getBaseContext() );
        Context ct = getBaseContext();
        float width = Display.widthDp(), height = Display.heightDp();


        builder.buildEntity( 2.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 2.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 2.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 2.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();

        builder.buildEntity( 4.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 4.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 4.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 4.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();

        builder.buildEntity( 6.0f, width*(6/12f) ).base( new OpponentEntity( ct, OpponentType.BOSS_1 ) ).create();

        builder.buildEntity( 9.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 9.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 9.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 12.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 12.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 14.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();
        builder.buildEntity( 14.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();

        builder.buildEntity( 16.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();
        builder.buildEntity( 16.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();

        builder.buildEntity( 19.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();
        builder.buildEntity( 19.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();
        builder.buildEntity( 19.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();

        builder.buildEntity( 21.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();
        builder.buildEntity( 21.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();

        builder.buildEntity( 23.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 23.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 23.0f, width*(7/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 23.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();

        builder.buildEntity( 25.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 25.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 25.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();
        builder.buildEntity( 25.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_1 ) ).create();

        builder.buildEntity( 27.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 27.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 27.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 30.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();
        builder.buildEntity( 30.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_2 ) ).create();

        builder.buildEntity( 32.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();
        builder.buildEntity( 32.0f, width*(10/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_3 ) ).create();

        builder.buildEntity( 34.0f, width*(1/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();
        builder.buildEntity( 34.0f, width*(11/12f) ).base( new OpponentEntity( ct, OpponentType.ATTACK_2 ) ).create();

        builder.buildEntity( 36.0f, width*(2/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();
        builder.buildEntity( 36.0f, width*(5/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();
        builder.buildEntity( 36.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.RAM_2 ) ).create();

        builder.buildEntity( 38.0f, width*(4/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();
        builder.buildEntity( 38.0f, width*(8/12f) ).base( new OpponentEntity( ct, OpponentType.BASIC_1 ) ).create();


        try{
            builder.saveAs( getResources().getString( R.string.level_filename_3 ) );
        }catch( IOException e ){
            Log.d( "MMA generateLevel3", e.getMessage() );
        }
    }
}
