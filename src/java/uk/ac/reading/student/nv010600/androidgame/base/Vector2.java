package uk.ac.reading.student.nv010600.androidgame.base;
import android.graphics.PointF;
import android.util.Log;

import java.io.Serializable;


public class Vector2 implements Serializable {

    // ----- Public ----------------------------------------------------------------------------- //
    public float x;
    public float y;


    public Vector2() {
        this( 0, 0 );
    }


    public Vector2( PointF point ) {
        this( point.x, point.y );
    }


    public Vector2( String str ) {
        String[] parts = str.split( " " );
        if( parts.length <= 2 ){
            x = 0; y = 0;
        }
        x = Float.parseFloat( parts[0] );
        y = Float.parseFloat( parts[1] );
    }


    public Vector2( Vector2 vec ) {
        this( vec.x, vec.y );
    }


    public Vector2( float x, float y ) {
        this.x = x;
        this.y = y;
    }


    public static Vector2 add( Vector2 a, Vector2 b ) {
        return new Vector2( a.x + b.x, a.y + b.y );
    }


    public static Vector2 sub( Vector2 a, Vector2 b ) {
        return new Vector2( a.x - b.x, a.y - b.y );
    }

    public static float distance( Vector2 a, Vector2 b ) {
        return (float)Math.sqrt( Math.pow( (a.x - b.x), 2 ) + Math.pow( (a.y - b.y), 2 ) );
    }


    public float length() {
        return (float)Math.sqrt( Math.pow( x, 2 ) + Math.pow( y, 2 ) );
    }


    public float radians() {
        if( x == 0 && y == 0 ) return 0;
        Vector2 b = clone();

        if( b.x == 0 ) b.x = 0.01f;
        if( b.y == 0 ) b.y = 0.01f;
        float rad = (float)Math.atan( b.y / b.x );

        float mod = 0;
        if( b.x >= 0 ){
            if( b.y >= 0 ) mod = 0;
            else           mod = 360 * (3.1415f/180);
        }else{
            if( b.y >= 0 ) mod = 180 * (3.1415f/180);
            else           mod = 180 * (3.1415f/180);
        }

        //Log.d( "Vector2", String.format( "%f %f %f %f %f", rad, mod, y/x, y, x ) );
        return mod + rad;
    }


    public float degrees() {
        return radians() * (180/3.1415f);
    }

    public Vector2 normalise() {
        float len = length();
        return new Vector2( x/len, y/len );
    }


    public static Vector2 normalise( float radians ) {
        return new Vector2( (float)Math.cos( radians ), (float)Math.sin( radians ) );
    }


    public Vector2 clone() {
        return new Vector2( this );
    }
}
