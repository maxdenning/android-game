package uk.ac.reading.student.nv010600.androidgame.game;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.Display;


public class ScrollingBackground {

    public ScrollingBackground( Context context ) {
        mBackground = BitmapFactory.decodeResource( context.getResources(), R.drawable.background );
        mPositionY = 0;
        mSpeed = context.getResources().getInteger( R.integer.init_background_scroll_speed );
        mPaint = new Paint( Paint.DITHER_FLAG | Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG );

        float wratio = Display.widthPx() / (float)mBackground.getWidth();
        mBackground = Bitmap.createScaledBitmap( mBackground, (int)(mBackground.getWidth()*wratio), (int)(mBackground.getHeight()*wratio), false );

        mNow = System.currentTimeMillis();
        mLastTime = mNow;
    }



    public void update( float elapsed ) {
        mPositionY += Display.dp2px( mSpeed ) * elapsed;
        mPositionY %= Display.heightPx();
    }


    public void draw( Canvas canvas ) {
        // Update
        mNow = System.currentTimeMillis();
        update( (mNow - mLastTime) / 1000.0f );
        mLastTime = mNow;

        // Draw
        int before = (int)Math.ceil( mPositionY / (float)mBackground.getHeight() );
        int after = (int)Math.ceil( (Display.heightPx() - mPositionY - mBackground.getHeight()) / (float)mBackground.getHeight() );

        canvas.drawBitmap( mBackground, 0, mPositionY, mPaint );
        for( int i = 1; i <= before; i++ ) canvas.drawBitmap( mBackground, 0, mPositionY - (i*mBackground.getHeight()), mPaint );
        for( int i = 1; i <= after;  i++ ) canvas.drawBitmap( mBackground, 0, mPositionY + (i*mBackground.getHeight()), mPaint );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private Bitmap mBackground;
    private Paint mPaint;
    private int mPositionY;             // pixels
    private int mSpeed;                 // dp per second

    private long mNow;
    private long mLastTime;
}
