package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;
import uk.ac.reading.student.nv010600.androidgame.game.upgrade.Upgrade;
import java.util.HashSet;


public class UpgradeReceiverAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public UpgradeReceiverAttributes() {
        mUpgrades = new HashSet<>();
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private HashSet<Upgrade> mUpgrades;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public HashSet<Upgrade> upgrades() { return mUpgrades; }
    public boolean addUpgrade( Upgrade upgrade ) { return mUpgrades.add( upgrade ); }
    public boolean removeUpgrade( Upgrade upgrade ) { return mUpgrades.remove( upgrade ); }
}
