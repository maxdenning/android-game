package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import android.util.Log;

import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Display;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SeekerAttributes;


public class SeekerControlBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public SeekerControlBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mMovement = parent.getAttribute( MovementAttributes.class );
        mSeeker = parent.getAttribute( SeekerAttributes.class );
        mCollider = parent.getAttribute( ColliderAttributes.class );
    }


    @Override
    public void update( float elapsed ) {
        mSeeker.setTimer( mSeeker.timer() + elapsed );

        // Wait until delay complete
        if( mSeeker.timer() < mSeeker.delay() )return;


        // Search for a target
        if( !mSeeker.foundTarget() ){
            for( Entity other : Entity.allEntities() ){
                if( !other.hasAttribute( MovementAttributes.class ) || !other.hasAttribute( ColliderAttributes.class ) || !other.hasAttribute( HealthAttributes.class ) ) continue;
                if( other.getAttribute( ColliderAttributes.class ).group() == mCollider.group() ) continue;

                Vector2 otherPos = other.getAttribute( MovementAttributes.class ).position();
                if( Vector2.distance( mMovement.position(), otherPos ) > Display.dp2px( mSeeker.searchRadius() ) ) continue;

                mSeeker.setTarget( other );
            }
            return;
        }


        // Stop seeking after length complete
        if( !mMovement.isMoving() || mSeeker.timer() > mSeeker.delay() + mSeeker.seekLength() ) return;


        // Move towards target
        Vector2 direction = mMovement.direction();
        Vector2 desired = Vector2.sub( mSeeker.target().getAttribute( MovementAttributes.class ).position(), mMovement.position() ).normalise();
        Vector2 delta = Vector2.sub( desired, direction );
        direction.x += (delta.x * mSeeker.turningSpeed() * elapsed);// + ((Math.random()-1)*elapsed);
        direction.y += (delta.y * mSeeker.turningSpeed() * elapsed);// + ((Math.random()-1)*elapsed);
        mMovement.setDirection( direction.normalise() );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient MovementAttributes mMovement;
    private transient SeekerAttributes mSeeker;
    private transient ColliderAttributes mCollider;
}
