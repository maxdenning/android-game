package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import android.util.Log;

import java.util.Random;

import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileAttributes.*;
import uk.ac.reading.student.nv010600.androidgame.game.entities.UpgradeEntity;
import uk.ac.reading.student.nv010600.androidgame.game.upgrade.ProjectileUpgrade;
import uk.ac.reading.student.nv010600.androidgame.game.upgrade.RepairUpgrade;
import uk.ac.reading.student.nv010600.androidgame.game.upgrade.Upgrade;


public class UpgradeDropBehaviour extends Behaviour {


    // ----- Public ----------------------------------------------------------------------------- //
    public UpgradeDropBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mMovement = parent.getAttribute( MovementAttributes.class );
        mCollider = parent.getAttribute( ColliderAttributes.class );
    }


    @Override
    public void destroy() {
        super.destroy();

        Random r = new Random( System.nanoTime() );
        float rf = r.nextFloat();
        Upgrade upgrade = null;


        // 80% of no drop, 10% chance of health, 10% chance of weapon

        if( rf > 0.9 ){
            // spawn health, in range 25 -> 75
            upgrade = new RepairUpgrade( r.nextInt( 50 ) + 25 );

        }else if( rf > 0.8 ){
            // spawn weapon upgrade
            boolean patternFlag = r.nextBoolean();
            ProjectileGeneratorAttributes.Generator generator = new ProjectileGeneratorAttributes().new Generator(
                    (r.nextInt( 10 ) < 9) ? ProjectileType.LASER : ProjectileType.MISSILE,
                    r.nextFloat()+0.1f, r.nextFloat()+0.3f, r.nextInt( 3 )+1, r.nextInt( 3 )+1,
                    patternFlag ? Pattern.PARALLEL : Pattern.RADIAL,
                    patternFlag ? (r.nextFloat()*32)+16 : (r.nextFloat()*135)+45,
                    new Vector2( 0, 0 )
            );
            upgrade = new ProjectileUpgrade( 5.0f, generator );

        }else{
            // spawn nothing
            return;
        }

        Entity.launch( new UpgradeEntity( parent.context(),mMovement.position(), mCollider.group(), upgrade ) );
    }


    @Override
    public void update( float elapsed ) {
        // do nothing
    }


    // ----- Private ---------------------------------------------------------------------------- //
    private transient MovementAttributes mMovement;
    private transient ColliderAttributes mCollider;
}
