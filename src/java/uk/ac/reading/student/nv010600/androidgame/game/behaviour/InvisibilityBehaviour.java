package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.DamageAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.InvincibilityAttributes;


public class InvisibilityBehaviour extends Behaviour /* implements ColliderAttributes.Listener*/ {

    // ----- Public ----------------------------------------------------------------------------- //
    public InvisibilityBehaviour( Entity parent ) {
        super( parent );
    }


    @Override
    public void initialise() {
        mInvincibility = parent.getAttribute( InvincibilityAttributes.class );
        mGraphic = parent.getAttribute(GraphicsAttributes.class );
        mVisibilityTimer = 0;

        //parent.getAttribute( ColliderAttributes.class ).addListener( this );
    }


    @Override
    public void update( float elapsed ) {
        if( !mInvincibility.isInvincible() ) return;

        mInvincibility.setTimer( mInvincibility.timer() + elapsed );
        mVisibilityTimer += elapsed;

        // Alternate graphic visibility
        if( mVisibilityTimer > 0.2f ){
            mGraphic.setVisible( !mGraphic.visible() );
            mVisibilityTimer = 0;
        }

        // Reset invincibility
        if( mInvincibility.timer() < mInvincibility.invincibilityTime() ) return;
        mInvincibility.setTimer( 0 );
        mInvincibility.setInvincible( false );
        mGraphic.setVisible( true );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient InvincibilityAttributes mInvincibility;
    private transient GraphicsAttributes mGraphic;
    private transient float mVisibilityTimer;
}
