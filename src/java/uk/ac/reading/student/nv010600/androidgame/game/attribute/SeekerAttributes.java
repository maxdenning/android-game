package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;


public class SeekerAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public SeekerAttributes() {
        this( 0, 0, 0, 0 );
    }


    public SeekerAttributes( float delay, float seekLength, int searchRadius, float turningSpeed ) {
        setDelay( delay );
        setSeekLength( seekLength );
        setSearchRadius( searchRadius );
        setTurningSpeed( turningSpeed );
        setTimer( 0 );
        setTarget( null );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private float mDelay;                   // Delay before beginning seek (seconds)
    private float mSeekLength;              // Length of time spend seeking (seconds)
    private float mTimer;                   // Timer controlling delay and length (seconds)
    private int mSearchRadius;              // Entity detection distance (dp)
    private Entity mTarget;                 // Target to move towards
    private float mTurningSpeed;            // Speed of direction change



    // ----- Public Accessors ------------------------------------------------------------------- //
    public float delay() { return mDelay; }
    public float seekLength() { return mSeekLength; }
    public float timer() { return mTimer; }
    public int searchRadius() { return mSearchRadius; }
    public Entity target() { return mTarget; }
    public boolean foundTarget() { return mTarget != null && mTarget.isValid(); }
    public float turningSpeed() { return mTurningSpeed; }

    public void setDelay( float delay ) { mDelay = Math.max( 0, delay ); }
    public void setSeekLength( float length ) { mSeekLength = Math.max( 0, length ); }
    public void setTimer( float timer ) { mTimer = timer; }
    public void setSearchRadius( int radius ) { mSearchRadius = Math.max( 0, radius ); }
    public void setTarget( Entity target ) { mTarget = target; }
    public void setTurningSpeed( float speed ) { mTurningSpeed = speed; }
}
