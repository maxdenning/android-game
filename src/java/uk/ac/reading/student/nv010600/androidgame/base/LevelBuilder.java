package uk.ac.reading.student.nv010600.androidgame.base;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;


public class LevelBuilder extends LevelManager {

    // ----- Public ----------------------------------------------------------------------------- //
    public LevelBuilder( Context context ) {
        super( context );
    }


    public void saveAs( String levelName ) throws IOException {
        if( levelName == null || levelName.isEmpty() ) throw new IllegalArgumentException( String.format( "Illegal level name: %s", levelName ) );

        String fullName = String.format( mContext.getResources().getString( R.string.level_filename_format ), levelName );
        String path = mContext.getFilesDir().getPath() + "/" + fullName;

        Log.d( "Builder", String.format( "SAVING: %s", path ) );

        FileOutputStream ofile = new FileOutputStream( path );
        ObjectOutputStream oobj = new ObjectOutputStream( ofile );
        oobj.writeObject( this );
        oobj.close();
        ofile.close();

        Log.d( "Builder", String.format( "SAVED: %s", path ) );
    }


    public LevelManager load( String levelName ) throws IOException, ClassNotFoundException, IllegalArgumentException {
        if( levelName == null || levelName.isEmpty() ) throw new IllegalArgumentException( String.format( "Illegal level name: %s", levelName ) );

        String fullName = String.format( mContext.getResources().getString( R.string.level_filename_format ), levelName );
        String path = mContext.getFilesDir().getPath() + "/" + fullName;

        Log.d( "Builder", String.format( "LOADING: %s", path ) );

        File levelFile = new File( path );
        if( !levelFile.exists() ) throw new FileNotFoundException( String.format( "Level does not exist: %s", levelName ) );

        FileInputStream ifile = new FileInputStream( levelFile );
        ObjectInputStream iobj = new ObjectInputStream( ifile );
        LevelManager level = (LevelManager)iobj.readObject();
        iobj.close();
        ifile.close();

        Log.d( "Builder", String.format( "LOADED: %s", path ) );

        return level;
    }


    public boolean levelExists( String levelName ) {
        String fullName = String.format( mContext.getResources().getString( R.string.level_filename_format ), levelName );
        String path = mContext.getFilesDir().getPath() + "/" + fullName;

        File levelFile = new File( path );
        return levelFile.exists();
    }


    public EntityBuilder buildEntity( float insertTime, Vector2 insertPosition ) {
        return new EntityBuilder( insertTime, insertPosition );
    }


    public EntityBuilder buildEntity( float insertTime, float insertPositionX ) {
        return buildEntity( insertTime, new Vector2( insertPositionX, -1 ) );
    }




    public class EntityBuilder {

        public EntityBuilder( float insertTime, Vector2 insertPosition ) {
            mInsertTime = insertTime;
            mInsertPosition = insertPosition.clone();
            mEntity = new Entity( LevelBuilder.this.mContext );
        }


        public EntityBuilder base( Entity baseEntity ) {
            mEntity = baseEntity;
            return this;
        }


        public <T extends Attribute> EntityBuilder add( T attribute ) {
            mEntity.addAttribute( attribute );
            return this;
        }


        public <T extends Behaviour> EntityBuilder add( Class<T> behaviour ) {
            try{
                Class[] ctrtypes = new Class[]{ Entity.class };
                mEntity.addBehaviour( behaviour.getDeclaredConstructor( ctrtypes ).newInstance( mEntity )  );

            }catch( NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e ){
                // do something
            }
            return this;
        }


        public void create() {
            if( !mEntityQueue.containsKey( mInsertTime ) ){
                mEntityQueue.put( mInsertTime, new ArrayList<Entity>() );
            }

            if( mEntity.hasAttribute( MovementAttributes.class ) ){

                // set spawn y position as graphic height/2
                if( mInsertPosition.y < 0 && mEntity.hasAttribute( GraphicsAttributes.class ) ){
                    mInsertPosition.y = -mEntity.getAttribute( GraphicsAttributes.class ).size().y / 2;
                }

                mEntity.getAttribute( MovementAttributes.class ).setPosition( Display.dp2px( mInsertPosition ) );
            }

            mEntityQueue.get( mInsertTime ).add( mEntity );
        }


        public Entity entity() { return mEntity; }


        private float mInsertTime;          // time at which the entity will spawn into the game (seconds)
        private Vector2 mInsertPosition;    // coordinates at which the entity will spawn (dp)
        private Entity mEntity;             // the entity being built
    }



    // ----- Private ---------------------------------------------------------------------------- //


}
