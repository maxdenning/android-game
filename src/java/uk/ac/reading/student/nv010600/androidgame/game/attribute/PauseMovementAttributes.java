package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;


public class PauseMovementAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public PauseMovementAttributes() {
        this( 0, 0 );
    }


    public PauseMovementAttributes( float delay, float pauseLength ) {
        setDelay( delay );
        setPauseLength( pauseLength );
        setTimer( 0 );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private float mDelay;            // Time before
    private float mPauseLength;
    private float mTimer;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public float delay() { return mDelay; }
    public float pauseLength() { return mPauseLength; }
    public float timer() { return mTimer; }

    public void setDelay( float delay ) { mDelay = delay; }
    public void setPauseLength( float length ) { mPauseLength = length; }
    public void setTimer( float time ) { mTimer = time; }
}
