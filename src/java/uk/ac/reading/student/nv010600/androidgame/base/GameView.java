package uk.ac.reading.student.nv010600.androidgame.base;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import uk.ac.reading.student.nv010600.androidgame.game.Game;


public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    // ----- Public ----------------------------------------------------------------------------- //
    public GameView( Context context, AttributeSet attrs ) {
        super( context, attrs );

        //Get the holder of the screen and register interest
        getHolder().addCallback( this );


        //Set up a handler for messages from GameThread
        mHandler = new Handler() {
            @Override
            public void handleMessage( Message m ){
                // None
                Log.d( "GameView", m.toString() );
            }
        };


    }


    //Used to release any resources.
    public void cleanup() {
        mThread.setRunning( false );
        mThread.cleanup();
        removeCallbacks( mThread );
        mThread = null;
        setOnTouchListener( null );
        getHolder().removeCallback( this );
    }


    //ensure that we go into pause state if we go out of focus
    @Override
    public void onWindowFocusChanged( boolean hasWindowFocus ) {
        if( mThread != null && !hasWindowFocus ) mThread.setState( GameThread.STATE_PAUSE ); //.pause();
    }


    @Override
    public void surfaceCreated( SurfaceHolder holder ) {
        if( mThread != null ){
            mThread.setRunning( true );

            if( mThread.getState() == Thread.State.NEW ){
                //Just start the new thread
                mThread.start();

            }else if( mThread.getState() == Thread.State.TERMINATED ){
                //Start a new thread
                //Should be this to update screen with old game: new GameThread(this, thread);
                //The method should set all fields in new thread to the value of old thread's fields

                /*mThread = new Game( this,  );
                mThread.setRunning( true );
                mThread.start();*/

            }
        }
    }


    //Always called once after surfaceCreated. Tell the GameThread the actual size
    @Override
    public void surfaceChanged( SurfaceHolder holder, int format, int width, int height ) {
        if( mThread!=null ) mThread.setSurfaceSize( width, height );
    }


    /*
     * Need to stop the GameThread if the surface is destroyed
     * Remember this doesn't need to happen when app is paused on even stopped.
     */
    @Override
    public void surfaceDestroyed( SurfaceHolder arg0 ) {
        boolean retry = true;
        if( mThread != null ) mThread.setRunning( false );


        //join the thread with this thread
        while( retry ){
            try{
                if( mThread != null ) mThread.join();
                retry = false;
            }
            catch( InterruptedException e ){
                //naughty, ought to do something...
            }
        }
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private volatile GameThread mThread;    //Handle communication from the GameThread to the View/Activity Thread
    private Handler mHandler;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public GameThread getThread() {
        return mThread;
    }

    public Handler getHandler() {
        return mHandler;
    }

    public void setHandler( Handler handler ) {
        mHandler = handler;
    }

    public void setThread( GameThread newThread ) {
        mThread = newThread;
        setOnTouchListener( newThread );
        setClickable( true );
        setFocusable( true );
    }
}
