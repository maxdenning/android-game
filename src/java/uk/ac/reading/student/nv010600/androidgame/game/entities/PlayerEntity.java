package uk.ac.reading.student.nv010600.androidgame.game.entities;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.*;
import uk.ac.reading.student.nv010600.androidgame.game.InitRes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.DamageAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.InvincibilityAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.UpgradeReceiverAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicGraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicMovementBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileGeneratorAttributes.*;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileAttributes.*;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.DamageBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.InvisibilityBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.PixelColliderBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.ProjectileGeneratorBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.TouchControlAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.TouchControlBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.UpgradeReceiverBehaviour;
import android.content.Context;


/**
 * Player entity, used for ease of creation. Has no special properties that cannot be accessed via
 * getAttribute.
 * Initialisation values can be found in res.values.init
 *
 * @author Max Denning
 */
public class PlayerEntity extends Entity {

    /**
     * Constructor, sets position and touch input source
     * @param context context
     * @param position starting position
     * @param touchInput source of touch control
     */
    public PlayerEntity( Context context, Vector2 position, TouchControlAttributes touchInput ) {
        super( context );
        InitRes i = new InitRes( context() );

        // Initialise projectile generators
        ProjectileGeneratorAttributes generators = addAttribute( new ProjectileGeneratorAttributes() );
        generators.addGenerator( generators.new Generator( ProjectileType.LASER, 0.2f, 0.4f, 3, 1, Pattern.PARALLEL, 0, new Vector2( 0, -32 ) ) );
        generators.addGenerator( generators.new Generator( ProjectileType.LASER, 0.2f, 0.4f, 3, 2, Pattern.RADIAL, 90, new Vector2( 0, -32 ) ) );


        // Add attributes
        addAttribute( new PlayerAttribute() );
        addAttribute( touchInput );
        addAttribute( new MovementAttributes( position, i.getFloat( R.raw.init_player_max_speed ), -1, new Vector2(0, 0), false ) );
        addAttribute( new GraphicsAttributes( R.drawable.player, i.getVector2( R.string.init_player_graphics_size ) ) );
        addAttribute( new ColliderAttributes( 1, null ) );
        addAttribute( new HealthAttributes( i.getInt( R.integer.init_player_max_hitpoints ) ) );
        addAttribute( new InvincibilityAttributes( false, i.getFloat( R.raw.init_player_invincibility_time ) ) );
        addAttribute( new DamageAttributes( i.getInt( R.integer.init_player_damage ) ) );                     // damage inflicted upon body hitting another entity
        addAttribute( new UpgradeReceiverAttributes() );


        // Add behaviours
        addBehaviour( new TouchControlBehaviour( this ) );
        addBehaviour( new BasicGraphicsBehaviour( this ) );
        addBehaviour( new ProjectileGeneratorBehaviour( this ) );
        addBehaviour( new BasicMovementBehaviour( this ) );
        addBehaviour( new PixelColliderBehaviour( this ) );
        addBehaviour( new DamageBehaviour( this ) );
        addBehaviour( new InvisibilityBehaviour( this ) );
        addBehaviour( new UpgradeReceiverBehaviour( this ) );
    }


    /**
     * Serves no purpose other than to indicate the player is the player
     */
    public static class PlayerAttribute extends Attribute {
    }
}
