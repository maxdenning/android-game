package uk.ac.reading.student.nv010600.androidgame.activities;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.LevelBuilder;


public class LevelCreatorActivity extends AppCompatActivity implements Button.OnClickListener, AdapterView.OnItemClickListener {


    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_level_creator );

        findViewById( R.id.fab_add_entity ).setOnClickListener( this );
        findViewById( R.id.fab_save_level ).setOnClickListener( this );

        mLevelBuilder = new LevelBuilder( getBaseContext() );
        mListAdapter = new CustomAdapter( this, new ArrayList<String>() );
        ((ListView)findViewById( R.id.lst_entities )).setAdapter( mListAdapter );
        ((ListView)findViewById( R.id.lst_entities )).setOnItemClickListener( this );

        // Show tutorial
        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( R.string.title_activity_level_creator )
                .setMessage( R.string.level_creator_tutorial_message )
                .setCancelable( true )
                .show();
    }


    @Override
    public void onClick( View source ) {
        switch( source.getId() ){
            case R.id.fab_save_level: onSaveClick( source ); break;
            case R.id.fab_add_entity: onAddClick( source ); break;
            default: break;
        }
    }


    @Override
    public void onItemClick( AdapterView a, View v, int i, long l) {
        final int selected = i;

        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( R.string.title_activity_level_creator )
               .setMessage( R.string.level_creator_remove_entity_message )
               .setCancelable( true )
               .setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        mListAdapter.removeItem( selected );
                    }
                } )
               .show();
    }


    public void onAddClick( View source ) {
        Intent creatorIntent = new Intent( getBaseContext(), EntityCreatorActivity.class );
        creatorIntent.putExtra( "builder", mLevelBuilder );
        startActivityForResult( creatorIntent, 0 /*REQUEST CODE*/ );
    }


    public void onSaveClick( View source ) {
        if( mLevelBuilder.queue().isEmpty() ){
            Toast.makeText( this, "No opponents defined", Toast.LENGTH_LONG ).show();
            return;
        }

        final EditText textInput = new EditText( this );

        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setTitle( "Save" )
                .setMessage( "Enter name of level:" )
                .setView( textInput )
                .setCancelable( true )
                .setPositiveButton( "Save & Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick( DialogInterface dialog, int which ) {
                        String input = textInput.getText().toString();
                        if( input.isEmpty() ) return;

                        try{
                            mLevelBuilder.saveAs( input );
                        }catch( IOException e ){
                            AlertDialog.Builder errd = new AlertDialog.Builder( getBaseContext() );
                            errd.setTitle( "Error" ).setMessage( e.getMessage() ).setCancelable( true ).show();
                            return;
                        }

                        startActivity( new Intent( getBaseContext(), MainMenuActivity.class ) );
                    }
                });
        builder.show();
    }


    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        if( resultCode != RESULT_OK || !data.hasExtra( "builder" ) ) return;

        mLevelBuilder = (LevelBuilder)data.getExtras().get( "builder" );
        mLevelBuilder.setContext( getBaseContext() );
        String info = (String)data.getExtras().get( "info" );
        int sprite_id = (int)data.getExtras().get( "sprite" );

        mListAdapter.addItem( info, sprite_id );
    }



    private LevelBuilder mLevelBuilder;
    private CustomAdapter mListAdapter;




    private class CustomAdapter extends ArrayAdapter<String> {

        public CustomAdapter( Context context, List<String> values ) {
            super( context, R.layout.listview_level_creator, values );
            mStringValues = values;
            mIconValues = new ArrayList<>();
        }


        @Override
        public View getView( int position, View convertView, ViewGroup parent ) {
            LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            View rowView = inflater.inflate( R.layout.listview_level_creator, parent, false );
            TextView text = (TextView)rowView.findViewById( R.id.txt_listview_info );
            ImageView icon = (ImageView)rowView.findViewById( R.id.imv_listview_sprite );

            text.setText( mStringValues.get( position ) );
            icon.setImageResource( mIconValues.get( position ) );

            return rowView;
        }


        public void addItem( String info, Integer iconID ) {
            mStringValues.add( info );
            mIconValues.add( iconID );
            notifyDataSetChanged();
        }


        public void removeItem( int position ) {
            mStringValues.remove( position );
            mIconValues.remove( position );
            notifyDataSetChanged();
        }



        private final List<String> mStringValues;
        private List<Integer> mIconValues;
    }
}
