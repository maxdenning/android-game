package uk.ac.reading.student.nv010600.androidgame.game.entities;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.Display;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.InitRes;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.DamageAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SeekerAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.SinusoidControlAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicGraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicMovementBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ProjectileAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.DamageBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.PixelColliderBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.RotatorGraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.SeekerControlBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.SinusoidControlBehaviour;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;


public class ProjectileEntity extends Entity implements ColliderAttributes.Listener {

    public ProjectileEntity( Context context, Vector2 position, ProjectileAttributes.ProjectileType type, int collisionGroup ) {
        super( context );
        InitRes i = new InitRes( context() );


        // Load in bitmaps
        if( BMP_LASER == null ){
            Vector2 size = i.getVector2( R.string.init_laser_graphics_size );
            BMP_LASER = Bitmap.createScaledBitmap( BitmapFactory.decodeResource( context.getResources(), R.drawable.laser ), (int)Display.dp2px( size.x ), (int)Display.dp2px( size.y ), false );
        }
        if( BMP_MISSILE == null ){
            Vector2 size = i.getVector2( R.string.init_missile_graphics_size );
            BMP_MISSILE = Bitmap.createScaledBitmap( BitmapFactory.decodeResource( context.getResources(), R.drawable.missile ), (int)Display.dp2px( size.x ), (int)Display.dp2px( size.y ), false );
        }



        addAttribute( new ColliderAttributes( collisionGroup, this ) );
        addBehaviour( new PixelColliderBehaviour( this ) );

        addAttribute( new ProjectileAttributes( type ) );

        addBehaviour( new DamageBehaviour( this ) );


        switch( type ){
            case LASER:
                //addAttribute( new GraphicsAttributes( R.drawable.laser, i.getVector2( R.string.init_laser_graphics_size ) ) );
                addAttribute( new GraphicsAttributes( BMP_LASER, i.getVector2( R.string.init_laser_graphics_size ) ) );

                addAttribute( new MovementAttributes( position, i.getFloat( R.raw.init_laser_max_speed ), i.getFloat( R.raw.init_laser_acceleration ), new Vector2(0, 1), true ) );
                addAttribute( new DamageAttributes( i.getInt( R.integer.init_laser_damage ) ) );

                addBehaviour( new BasicMovementBehaviour( this ) );
                addBehaviour( new RotatorGraphicsBehaviour( this ) );

                break;


            case MISSILE:
                //addAttribute( new GraphicsAttributes( R.drawable.missile, i.getVector2( R.string.init_missile_graphics_size ) ) );
                addAttribute( new GraphicsAttributes( BMP_MISSILE, i.getVector2( R.string.init_missile_graphics_size ) ) );

                addAttribute( new MovementAttributes( position, i.getFloat( R.raw.init_missile_max_speed ), i.getFloat( R.raw.init_missile_acceleration ), new Vector2(0, 1), true ) );
                addAttribute( new DamageAttributes( i.getInt( R.integer.init_missile_damage ) ) );
                addAttribute( new SeekerAttributes( i.getFloat( R.raw.init_missile_seek_delay ), i.getFloat( R.raw.init_missile_seek_length ), i.getInt( R.integer.init_missile_seek_radius ), i.getFloat( R.raw.init_missile_seek_turning_speed ) ) );

                addBehaviour( new SeekerControlBehaviour( this ) );
                addBehaviour( new BasicMovementBehaviour( this ) );
                addBehaviour( new RotatorGraphicsBehaviour( this ) );
                break;
        }
    }


    @Override
    public void onCollision( Entity other ) {
        if( other.hasAttribute( HealthAttributes.class ) ) destroy();
    }


    private static Bitmap BMP_LASER = null;
    private static Bitmap BMP_MISSILE = null;
}
