package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import java.util.HashSet;
import android.graphics.Color;
import android.support.annotation.Nullable;


public class ColliderAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public static final int COLOUR_KEY = Color.TRANSPARENT;
    public static final int DEFAULT_GROUP = -1;


    public ColliderAttributes() {
        this( DEFAULT_GROUP, null );
    }


    public ColliderAttributes( int group, @Nullable Listener collisionListener ) {
        if( mWaitingColliders == null ) mWaitingColliders = new HashSet<>();
        if( mProcessedColliders == null ) mProcessedColliders = new HashSet<>();
        setGroup( group );
        mCollisionListeners = new HashSet<>();
        if( collisionListener != null ) addListener( collisionListener );
    }


    public interface Listener {
        void onCollision( Entity other );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private int mGroup;
    private HashSet<Listener> mCollisionListeners;

    private static HashSet<Entity> mWaitingColliders;     // constant time for add, remove, and contains, Set means unique entries
    private static HashSet<Entity> mProcessedColliders;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public int group() { return mGroup; }
    public HashSet<Listener> listeners() { return mCollisionListeners; }
    public static HashSet<Entity> waitingColliders() { return mWaitingColliders; }
    public static HashSet<Entity> processedColliders() { return mProcessedColliders; }

    public void setGroup( int group ) { mGroup = group; }
    public void setListeners( HashSet<Listener> collisionListeners ) { mCollisionListeners = collisionListeners; }
    public static void setWaitingColliders( HashSet<Entity> waitingColliders ) { mWaitingColliders = waitingColliders; }
    public static void setProcessedColliders( HashSet<Entity> processedColliders ) { mProcessedColliders = processedColliders; }

    public void addListener( Listener collisionListener ) { mCollisionListeners.add( collisionListener ); }
    public void removeListener( Listener collisionListener ) { mCollisionListeners.remove( collisionListener ); }

    public static boolean addWaitingCollider( Entity collider ) { return mWaitingColliders.add( collider ); }
    public static boolean removeWaitingCollider( Entity collider ) { return mWaitingColliders.remove( collider ); }
    public static void clearWaitingColliders() { mWaitingColliders.clear(); }

    public static boolean addProcessedCollider( Entity collider ) { return mProcessedColliders.add( collider ); }
    public static boolean removeProcessedCollider( Entity collider ) { return mProcessedColliders.remove( collider ); }
    public static void clearProcessedColliders() { mProcessedColliders.clear(); }
}
