package uk.ac.reading.student.nv010600.androidgame.game.entities;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.InitRes;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.GraphicsAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.MovementAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ScoreAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.UpgradeReceiverAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicGraphicsBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.BasicMovementBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.behaviour.PixelColliderBehaviour;
import uk.ac.reading.student.nv010600.androidgame.game.upgrade.Upgrade;
import android.content.Context;


public class UpgradeEntity extends Entity implements ColliderAttributes.Listener {

    // ----- Public ----------------------------------------------------------------------------- //
    public UpgradeEntity( Context context, Vector2 position, int colliderGroup, Upgrade upgrade ) {
        super( context );
        InitRes i = new InitRes( context );
        mUpgrade = upgrade;

        // Attributes
        addAttribute( new MovementAttributes( position, (float)i.getInt( R.integer.init_background_scroll_speed ), -1, new Vector2( 0, 1 ), true ) );
        addAttribute( new GraphicsAttributes( upgrade.graphicResource(), i.getVector2( R.string.init_upgrade_graphics_size ) ) );
        addAttribute( new ScoreAttributes( i.getInt( R.integer.init_upgrade_score ) ) );
        addAttribute( new ColliderAttributes( colliderGroup, this ) );

        // Behaviours
        addBehaviour( new PixelColliderBehaviour( this ) );
        addBehaviour( new BasicGraphicsBehaviour( this ) );
        addBehaviour( new BasicMovementBehaviour( this ) );
    }


    @Override
    public void onCollision( Entity other ) {
        if( !other.hasAttribute( UpgradeReceiverAttributes.class ) ) return;

        other.getAttribute( UpgradeReceiverAttributes.class ).addUpgrade( mUpgrade );
        ScoreAttributes.setTotalScore( ScoreAttributes.totalScore() + getAttribute( ScoreAttributes.class ).score() );
        destroy();
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private Upgrade mUpgrade;
}
