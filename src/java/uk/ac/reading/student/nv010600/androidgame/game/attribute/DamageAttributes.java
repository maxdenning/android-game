package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;


/**
 * Contains properties relevant to the on-touch damage caused by the parent entity.
 *
 * @author Max Denning
 */
public class DamageAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    /**
     * Default constructor, sets damage to 0
     */
    public DamageAttributes() {
        this( 0 );
    }


    /**
     * Constructor, sets damage
     * @param damage damage value
     */
    public DamageAttributes( int damage ) {
        setDamage( damage );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    /** Damage to be inflicted on other entities on contact */
    public int mDamage;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public int damage() { return mDamage; }
    public void setDamage( int damage ) { mDamage = damage; }
}
