package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;


public class MovementAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public MovementAttributes() {
        this( new Vector2( 0, 0 ), 0, 0, new Vector2( 0, 0 ), false );
    }


    public MovementAttributes( Vector2 position ) {
        this( position, 0, 0, new Vector2( 0, 0 ), false );
    }


    public MovementAttributes( Vector2 position, float maxSpeed, float acceleration, Vector2 direction, boolean isMoving ) {
        setPosition( position );
        setMaxSpeed( maxSpeed );
        setAcceleration( acceleration );
        setSpeed( 0 );
        setDirection( direction );
        setIsMoving( isMoving );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private Vector2 mPosition;                // position of entity (pixel x pixel)
    private float mMaxSpeed;                  // maximum speed  (dp per second)
    private float mAcceleration;              // current acceleration (dp per second per second)
    private float mSpeed;                     // current speed, maximum value is maxSpeed (dp per second)
    private Vector2 mDirection;               // normalised vector describing axis magnitude
    private boolean mIsMoving;                // true if currently in motion



    // ----- Public Accessors ------------------------------------------------------------------- //
    public synchronized Vector2 position() { return mPosition; }
    public synchronized float   maxSpeed() { return mMaxSpeed; }
    public synchronized float   acceleration() { return mAcceleration; }
    public synchronized float   speed() { return mSpeed; }
    public synchronized Vector2 direction() { return mDirection; }
    public synchronized boolean isMoving() { return mIsMoving; }

    public synchronized void setPosition( Vector2 position ) { mPosition = position.clone(); }
    public synchronized void setPosition( float x, float y ) { mPosition.x = x; mPosition.y = y; }
    public synchronized void setMaxSpeed( float maxSpeed ) { mMaxSpeed = Math.max( 0, maxSpeed ); }
    public synchronized void setAcceleration( float acceleration ) { mAcceleration = acceleration; }
    public synchronized void setSpeed( float speed ) { mSpeed = Math.max( 0, Math.min( speed, maxSpeed() ) ); }
    public synchronized void setDirection( Vector2 direction ) { mDirection = direction.clone(); }
    public synchronized void setDirection( float x, float y ) { mDirection.x = x; mDirection.y = y; }
    public synchronized void setIsMoving( boolean isMoving ) { mIsMoving = isMoving; }
}
