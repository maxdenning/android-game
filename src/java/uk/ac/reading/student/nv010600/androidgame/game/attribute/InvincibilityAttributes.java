package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;


public class InvincibilityAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public InvincibilityAttributes() {
        this( false, 0 );
    }

    public InvincibilityAttributes( boolean isInvincible, float maxTime ) {
        setInvincible( isInvincible );
        setInvincibilityTime( maxTime );
        mTimer = 0;
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private boolean mInvincible;
    private float mInvincibilityTime;
    private float mTimer;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public boolean isInvincible() { return mInvincible; }
    public float invincibilityTime() { return mInvincibilityTime; }
    public float timer() { return mTimer; }

    public void setInvincible( boolean invincible ) { mInvincible = invincible; }
    public void setInvincibilityTime( float maxTime ) { mInvincibilityTime = maxTime; }
    public void setTimer( float time ) { mTimer = time; }

}
