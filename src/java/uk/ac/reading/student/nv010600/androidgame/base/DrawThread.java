package uk.ac.reading.student.nv010600.androidgame.base;
import android.graphics.Canvas;
import android.view.SurfaceHolder;

import java.util.ArrayList;


public class DrawThread extends Thread {

    // ----- Public ----------------------------------------------------------------------------- //
    public DrawThread( GameThread game, SurfaceHolder holder ) {
        mHolder = holder;
        mGame = game;
    }


    @Override
    public void run() {
        Canvas canvas;

        while( mGame.isRunning() ){
            canvas = null;
            try{
                canvas = mHolder.lockCanvas( null );
                mGame.draw( canvas );

            }finally{
                if( canvas != null && mHolder != null ) mHolder.unlockCanvasAndPost( canvas );
            }
        }
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private SurfaceHolder mHolder;
    private GameThread mGame;
}
