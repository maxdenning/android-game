package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;


public class SinusoidControlAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public SinusoidControlAttributes() {
        this( 0, 0 );
    }


    public SinusoidControlAttributes( float amplitude, float frequency ) {
        setTimer( 0 );
        setAmplitude( amplitude );
        setFrequency( frequency );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private float mTimer;
    private float mAmplitude;
    private float mFrequency;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public float timer() { return mTimer; }
    public float amplitude() { return mAmplitude; }
    public float frequency() { return mFrequency; }

    public void setTimer( float time ) { mTimer = time; }
    public void setAmplitude( float amp ) { mAmplitude = amp; }
    public void setFrequency( float period ) { mFrequency = period; }
}
