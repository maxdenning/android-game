package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import android.graphics.Canvas;
import java.util.ArrayList;
import java.util.HashSet;


public abstract class GraphicsBehaviour extends Behaviour {

    // ----- Public ----------------------------------------------------------------------------- //
    public GraphicsBehaviour( Entity parent ) {
        super( parent );
        synchronized( mLock ){
            if( ALL_GRAPHICS == null ) ALL_GRAPHICS = new HashSet<>();
        }
    }

    @Override
    public void initialise() {
        synchronized( mLock ){
            ALL_GRAPHICS.add( this );
        }
    }


    @Override
    public void destroy() {
        synchronized( mLock ){
            ALL_GRAPHICS.remove( this );
        }
    }


    @Override
    public final void update( float elapsed ) {}        // not required


    public abstract void draw( Canvas canvas );


    public static void drawAll( Canvas canvas ) {
        synchronized( mLock ){
            if( GraphicsBehaviour.ALL_GRAPHICS != null ){
                for( GraphicsBehaviour graphic : GraphicsBehaviour.ALL_GRAPHICS ){
                    graphic.draw( canvas );
                }
            }
        }
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient static HashSet<GraphicsBehaviour> ALL_GRAPHICS = null;
    private transient static final Integer mLock = 0;
}
