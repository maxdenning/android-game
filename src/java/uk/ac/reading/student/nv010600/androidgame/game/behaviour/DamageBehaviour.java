package uk.ac.reading.student.nv010600.androidgame.game.behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Behaviour;
import uk.ac.reading.student.nv010600.androidgame.base.Entity;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ColliderAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.DamageAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.HealthAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.InvincibilityAttributes;
import uk.ac.reading.student.nv010600.androidgame.game.attribute.ScoreAttributes;


/**
 * Uses values present in DamageAttribute to create the effect of dealing damage on touch.
 *
 * @author Max Denning
 */
public class DamageBehaviour extends Behaviour implements ColliderAttributes.Listener {

    // ----- Public ----------------------------------------------------------------------------- //
    /**
     * Constructor, sets parent
     * @param parent parent entity of behaviour
     */
    public DamageBehaviour( Entity parent ) {
        super( parent );
    }


    /**
     * @see Behaviour#initialise()
     */
    @Override
    public void initialise() {
        mHealth = parent.getAttribute( HealthAttributes.class );
        mDamage = parent.getAttribute( DamageAttributes.class );
        mCollider = parent.getAttribute( ColliderAttributes.class );
        mInvincibility = parent.getAttribute( InvincibilityAttributes.class );

        mCollider.addListener( this );
    }


    /**
     * Checks if parent entity should be destroyed
     * @param elapsed   time elapsed since last update call
     */
    @Override
    public void update( float elapsed ) {
        if( mHealth != null && mHealth.hitPoints() <= 1 ) parent.destroy();
    }


    /**
     * Collision listener. Inflicts damage when the parent interacts with another entity.
     * Takes invincibility and health-less entities into account.
     * @param other the entity that has been collided with
     */
    @Override
    public void onCollision( Entity other ) {
        if( !other.hasAttribute( HealthAttributes.class ) ) return;
        if( other.hasAttribute( InvincibilityAttributes.class ) && other.getAttribute( InvincibilityAttributes.class ).isInvincible() ) return;
        if( mInvincibility != null && mInvincibility.isInvincible() ) return;

        // remove hitpoints from other entity
        HealthAttributes otherHealth = other.getAttribute( HealthAttributes.class );
        otherHealth.setHitPoints( otherHealth.hitPoints() - mDamage.damage() );
        if( other.hasAttribute( InvincibilityAttributes.class ) ) other.getAttribute( InvincibilityAttributes.class ).setInvincible( true );

        // if hitpoints below zero, destroy and add points to score
        if( otherHealth.hitPoints() <= 0 ){
            if( other.hasAttribute( ScoreAttributes.class ) ) ScoreAttributes.setTotalScore( ScoreAttributes.totalScore() + other.getAttribute( ScoreAttributes.class ).score() );
            other.destroy();
        }
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private transient HealthAttributes mHealth;
    private transient DamageAttributes mDamage;
    private transient ColliderAttributes mCollider;
    private transient InvincibilityAttributes mInvincibility;
}
