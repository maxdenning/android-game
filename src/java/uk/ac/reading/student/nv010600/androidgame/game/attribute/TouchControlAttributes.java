package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;
import android.view.MotionEvent;


public class TouchControlAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public TouchControlAttributes() {
        setLastEvent( null );
        setDestination( new Vector2( 0, 0 ) );
    }



    // ----- Private ---------------------------------------------------------------------------- //
    private MotionEvent mLastEvent;               // last touch event
    private Vector2 mDestination;                 // coordinates of destination (pixel x pixel)



    // ----- Public Accessors ------------------------------------------------------------------- //
    public MotionEvent lastEvent() { return mLastEvent; }
    public Vector2     destination() { return mDestination; }

    public void setLastEvent( MotionEvent event ) { mLastEvent = event; }
    public void setDestination( Vector2 destination ) { mDestination = destination.clone(); }
    public void setDestination( float x, float y ) { mDestination.x = x; mDestination.y = y; }
}
