package uk.ac.reading.student.nv010600.androidgame.game.attribute;
import uk.ac.reading.student.nv010600.androidgame.base.Attribute;
import uk.ac.reading.student.nv010600.androidgame.R;
import uk.ac.reading.student.nv010600.androidgame.base.Vector2;

import android.graphics.Bitmap;


public class GraphicsAttributes extends Attribute {

    // ----- Public ----------------------------------------------------------------------------- //
    public GraphicsAttributes() {
        this( R.drawable.placeholder, new Vector2( 16, 16 ) );
    }


    public GraphicsAttributes( int resourceID, Vector2 size ) {
        setGraphic( null );
        setResourceID( resourceID );
        setSize( size );
        setVisible( true );
    }

    public GraphicsAttributes( Bitmap graphic, Vector2 size ) {
        setGraphic( graphic );
        setResourceID( 0 );
        setSize( size );
        setVisible( true );
    }


    // ----- Private ---------------------------------------------------------------------------- //
    private Bitmap mGraphic;
    private int mResourceID;
    private Vector2 mSize;           // dp
    private boolean mVisible;



    // ----- Public Accessors ------------------------------------------------------------------- //
    public synchronized Bitmap graphic() { return mGraphic; }
    public synchronized int resourceID() { return mResourceID; }
    public synchronized Vector2 size() { return mSize; }
    public synchronized boolean visible() { return  mVisible; }

    public synchronized void setGraphic( Bitmap graphic ) { mGraphic = graphic; }
    public synchronized void setResourceID( int resourceID ) { mResourceID = resourceID; }
    public synchronized void setSize( Vector2 size ) { mSize = size; }
    public synchronized void setVisible( boolean visible ) { mVisible = visible; }
}
