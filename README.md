# Android Game
A 2D scrolling "doodle" style shoot 'em up game for Android.
Produced in March 2017 as part of a university module assigment.

## Gameplay
![Gamplay example 1](https://thumbs.gfycat.com/HelpfulSpryFinwhale-mobile.mp4) ![Gameplay example 2](https://thumbs.gfycat.com/NecessaryThirdDungbeetle-mobile.mp4)
